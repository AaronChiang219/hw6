// DATA TO LOAD
var work;
var daysOfWeek;
var redInc;
var greenInc;
var blueInc;

function Work(hSemester, hProjects) {
    this.semester = hSemester;
    this.projects = hProjects;
}
function Project(hName, hStudents, hLink) {
    this.name = hName;
    this.students = hStudents;
    this.link = hLink;
}
function initProjects() {
    var dataFile = "./js/TeamsAndStudents.json";
    loadData(dataFile);
}
function loadData(jsonFile) {
    $.getJSON(jsonFile, function (json) {
        loadJSONData(json);
        addProjects();
    });
}
function loadJSONData(data) {
    // LOAD Projects DATA
    work = new Array();
    for (var i = 0; i < data.team.length; i++) {
        var workData = data.team[i];
		var pStudents = new Array();
		for (var j = 0; j < data.students.length; j++){
			var student = data.students[j];
			if (student.team == workData.name){
				pStudents.push(student);
			}
		}
		var project = new Project(workData.name, pStudents, workData.link);
		work[i] = project;
    }
}

function addProjects() {
    var div = $("#project_tables");
	var text = "<h3>" + " Projects</h3>"
                + "<table><tbody>"; 
    for (var i = 0; i < work.length; i++) {
        var project = work[i];       
		text += "<tr>";
		text += getProjectCell(project);
		if ((i + 1) < work.length) {
                project = work[i + 1];
                text += getProjectCell(project);
        }
        text += "</tr>"; 
    }
	text += "</tbody></table><br /><br />";
	div.append(text);
}
function getProjectCell(project) {
    var text = "<td><a href=\""
            + project.link
            + "\"><img src=\"./images/projects/"
            + project.name.replace(/\s/g, '')
            + ".png\" /></a><br />"
            + "<a href=\""
            + project.link
            + "\">" + project.name + "</a><br />"
            + "by ";
    for (var k = 0; k < project.students.length; k++) {
        text += project.students[k].fname + " " + project.students[k].lname;
        if ((k + 1) < project.students.length)
            text += ", ";
    }
    text += "<br /><br /></td>";
    return text;
}