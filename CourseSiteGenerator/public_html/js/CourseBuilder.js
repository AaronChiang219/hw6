/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//Data to load
var course;
var number;
var year;
var semester;
var title;
var home;
var instructor;

function buildBanner() {
    var dataFile = "./js/CourseData.json";
    somethingData(dataFile, loadBanner);
}

function somethingData(jsonFile, callback) {
    $.getJSON(jsonFile, function (json) {
        callback(json);
    });
}

function loadBanner(data){
    var Banner = $("#banner");
    course = data.course;
    number = data.number;
    year = data.year;
    semester = data.semester;
    title = data.title;
    var text = course + " " + number + 
               " - " + semester + " " + year +
               "<br>" + title;
    Banner.prepend(text);
	var Navbar = $("#navbar");
	home = data.home;
	var texts = "<a href \"" + home + "\">" +
	"<img class=\"sbu_navbar\" alt=\"Stony Brook University\"  src=\"./images/Banner.png\"> </a>" +
	"<a id=\"home_link\" class=\"open_nav\" href=\"index.html\">Home</a>" +
	"<a id=\"syllabus_link\" class=\"nav\" href=\"syllabus.html\">Syllabus</a>" +
	"<a id=\"schedule_link\" class=\"nav\" href=\"schedule.html\">Schedule</a>" +
	"<a id=\"hws_link\" class=\"nav\" href=\"hws.html\">HWs</a>" +
	"<a id=\"projects_link\" class=\"nav\" href=\"projects.html\">Projects</a>";
	Navbar.prepend(texts);
	var Bottom = $("#desc");
	instructor = data.instructor
	var textes = "<a href=\"http://www.stonybrook.edu\"><img alt=\"SBU\" class=\"sunysb\" src=\"./images/LFooter.jpg\" style=\"float:left\"></a>" +
    "<a href=\"http://www.cs.stonybrook.edu\"><img alt=\"CS\" src=\"./images/RFooter.png\" style=\"float:right\"></a>"+
	"<p style=\"font-size:9pt; text-align:center;\">Web page created and maintained<br>"+
    "by <span id=\"instructor_link\"><a href=\"http://www.cs.stonybrook.edu/~richard\">" + instructor +"</a></span>"+
	"<br><br><br><br></p>";
	Bottom.append(textes);
}


