/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cm.workspace;
import cm.data.CourseData;
import cm.data.CourseTemp;
import csg.data.AppData;
import csg.workspace.AppWorkspace;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.scene.control.Button;
import djf.controller.AppFileController;
import static djf.settings.AppPropertyType.EXPORT_WORK_TITLE;
import static djf.settings.AppStartupConstants.PATH_EMPTY;
import static djf.settings.AppStartupConstants.PATH_HTML;
import static djf.settings.AppStartupConstants.PATH_WORK;
import djf.ui.AppGUI;
import static tam.CourseSiteGeneratorProp.*;
import djf.ui.AppMessageDialogSingleton;
import djf.ui.AppYesNoCancelDialogSingleton;
import java.io.File;
import java.io.FilenameFilter;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import jtps.jTPS;
import jtps.jTPS_Transaction;
import properties_manager.PropertiesManager;
import tam.CourseSiteGeneratorApp;
/**
 *
 * @author caaro
 */
public class CourseController {
    CourseSiteGeneratorApp app;
    
    public CourseController(CourseSiteGeneratorApp initApp){
        app = initApp;
    }
    
    private void markWorkAsEdited(){
        AppGUI gui = app.getGUI();
        gui.getFileController().markAsEdited(gui);
    }
    
    public void handleChangeTemplate(){
        CourseWorkspace workspace = ((AppWorkspace)app.getWorkspaceComponent()).getCourseWorkspace();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        DirectoryChooser dc = new DirectoryChooser();
        dc.setInitialDirectory(new File(PATH_EMPTY));
        dc.setTitle(props.getProperty(EXPORT_WORK_TITLE));
        File selectedFile = dc.showDialog(app.getGUI().getWindow());
        //now we have to populate the table with valid files
        File indexFile = new File(selectedFile.getPath()+"\\index.html");
        File syllabusFile = new File(selectedFile.getPath()+"\\syllabus.html");
        File scheduleFile = new File(selectedFile.getPath()+"\\schedule.html");
        File hwFile = new File(selectedFile.getPath()+"\\hws.html");
        File projectFile = new File(selectedFile.getPath()+"\\projects.html");
        File scriptFolder = new File(selectedFile.getPath()+"\\js");
        File cssFolder = new File(selectedFile.getPath()+"\\css");
        if (indexFile.exists() && syllabusFile.exists() && scheduleFile.exists() && hwFile.exists() && projectFile.exists() && cssFolder.exists()){
            TableView fileTable = workspace.getPageGrid();
            CourseData data = ((AppData)app.getDataComponent()).getCourseData();
            data.setSourceFolder(selectedFile);
            data.getTemplate().clear();
            data.getCSS().clear();
            data.addTemp("Home", "index.html", "HomeBuilder.js", Boolean.TRUE);
            data.addTemp("Syllabus", "syllabus.html", "SyllabusBuilder.js", Boolean.TRUE);
            data.addTemp("Schedule", "schedule.html", "ScheduleBuilder.js", Boolean.TRUE);
            data.addTemp("HWs", "hws.html", "HWsBuilder.js", Boolean.TRUE);
            data.addTemp("Projects", "projects.html", "ProjectsBuilder.js", Boolean.TRUE);
            FilenameFilter filter = new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    if (name.lastIndexOf('.')>0){
                        int lastIndex = name.lastIndexOf('.');
                        String ext = name.substring(lastIndex);
                        if (ext.equals(".css")){
                            return true;
                        }
                    }
                    return false;
                }
            };
            File[] css = cssFolder.listFiles(filter);
            for(File f: css){
                data.addCSS(f.getName());
            }
            markWorkAsEdited();
        } else {
            System.out.print("test");
        }
        
    }
    
    public void handleCheckBox(){
        CourseWorkspace workspace = ((AppWorkspace) app.getWorkspaceComponent()).getCourseWorkspace();
        TableView fileTable = workspace.getPageGrid();
        Object selectedItem = fileTable.getSelectionModel().getSelectedItem();
        if(selectedItem == null){
            return;
        }
        CourseTemp temp = (CourseTemp)selectedItem;
        temp.setUse(!temp.getUse());
        ((TableColumn)fileTable.getColumns().get(0)).setVisible(false);
        ((TableColumn)fileTable.getColumns().get(0)).setVisible(true);
        markWorkAsEdited();
    }
    
    public void handleChangeBanner(){
        CourseWorkspace workspace = ((AppWorkspace)app.getWorkspaceComponent()).getCourseWorkspace();
        CourseData data = ((AppData)app.getDataComponent()).getCourseData();
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File(PATH_WORK));
        fc.setTitle("choose image");
        File selectedFile = fc.showOpenDialog(app.getGUI().getWindow());
        ImageView banner = workspace.getBannerView();
        Image fileImage = banner.getImage();
        try {
            fileImage = new Image(selectedFile.toURI().toURL().toExternalForm());
            data.setBanner(fileImage);
            markWorkAsEdited();
        } catch (MalformedURLException ex) {
            Logger.getLogger(CourseController.class.getName()).log(Level.SEVERE, null, ex);
        }
        banner.setImage(fileImage);
        banner.setFitWidth(200);
        banner.setFitHeight(30);

        
    }
    
    public void handleChangeLFooter(){
        CourseWorkspace workspace = ((AppWorkspace)app.getWorkspaceComponent()).getCourseWorkspace();
        CourseData data = ((AppData)app.getDataComponent()).getCourseData();
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File(PATH_WORK));
        fc.setTitle("choose image");
        File selectedFile = fc.showOpenDialog(app.getGUI().getWindow());
        ImageView lfooter = workspace.getLFooterView();
        Image fileImage = lfooter.getImage();
        try {
            fileImage = new Image(selectedFile.toURI().toURL().toExternalForm());
            data.setLFooter(fileImage);
            markWorkAsEdited();
        } catch (MalformedURLException ex) {
            Logger.getLogger(CourseController.class.getName()).log(Level.SEVERE, null, ex);
        }
        lfooter.setImage(fileImage);
        lfooter.setFitWidth(200);
        lfooter.setFitHeight(30);
    }
    public void handleChangeRFooter(){
        CourseData data = ((AppData)app.getDataComponent()).getCourseData();
        CourseWorkspace workspace = ((AppWorkspace) app.getWorkspaceComponent()).getCourseWorkspace();
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File(PATH_WORK));
        fc.setTitle("choose image");
        File selectedFile = fc.showOpenDialog(app.getGUI().getWindow());
        ImageView rfooter = workspace.getRFooterView();
        Image fileImage = rfooter.getImage();
        try {
            fileImage = new Image(selectedFile.toURI().toURL().toExternalForm());
            data.setRFooter(fileImage);
            markWorkAsEdited();
        } catch (MalformedURLException ex) {
            Logger.getLogger(CourseController.class.getName()).log(Level.SEVERE, null, ex);
        }
        rfooter.setImage(fileImage);
        rfooter.setFitWidth(200);
        rfooter.setFitHeight(30);
    }
    public void handleChangeInfo(){
        CourseWorkspace workspace = ((AppWorkspace) app.getWorkspaceComponent()).getCourseWorkspace();
        CourseData data = ((AppData)app.getDataComponent()).getCourseData();
        TextField subTextField = workspace.getsubCombo();
        TextField semTextField = workspace.getsemCombo();
        TextField numTextField = workspace.getnumCombo();
        TextField yearTextField = workspace.getyearCombo();
        TextField titleTextField = workspace.getTitleTextField();
        TextField instructorTextField = workspace.getInstructorTextField();
        TextField homeTextField = workspace.getHomeTextField();
        String title = titleTextField.getText();
        String sub = subTextField.getText();
        String sem = semTextField.getText();
        String num = numTextField.getText();
        String year = yearTextField.getText();
        String instr = instructorTextField.getText();
        String home = homeTextField.getText();
        data.setCourseInfo(sub, num, sem, title, year, instr, home);
        markWorkAsEdited();
    }
    public void handleChangeDirectory(){
        CourseWorkspace workspace = ((AppWorkspace) app.getWorkspaceComponent()).getCourseWorkspace();
        CourseData data = ((AppData) app.getDataComponent()).getCourseData();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        DirectoryChooser dc = new DirectoryChooser();
        dc.setInitialDirectory(new File(PATH_EMPTY));
        dc.setTitle(props.getProperty(EXPORT_WORK_TITLE));
        File selectedFile = dc.showDialog(app.getGUI().getWindow());
        workspace.getExportDir().setText(selectedFile.getPath());
        data.setExport(selectedFile);
        markWorkAsEdited();
    }
    public void handleChangeCSS(){
        CourseWorkspace workspace = ((AppWorkspace) app.getWorkspaceComponent()).getCourseWorkspace();
        CourseData data = ((AppData) app.getDataComponent()).getCourseData();
        String cssname = (String) workspace.getstyleCombo().getValue();
        data.setmyCSS(new File(data.getSourceFolder().getPath()+"\\css\\"+cssname));
        markWorkAsEdited();
    }
}
