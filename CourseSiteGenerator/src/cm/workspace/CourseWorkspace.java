/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cm.workspace;

import cm.data.CourseData;
import cm.data.CourseTemp;
import csg.data.AppData;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.collections.FXCollections;
import tam.CourseSiteGeneratorApp;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ComboBox;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import jtps.jTPS;
import properties_manager.PropertiesManager;
import djf.components.AppWorkspaceComponent;
import java.awt.Color;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.TilePane;
import javafx.util.Callback;
import javax.swing.BorderFactory;
import tam.CourseSiteGeneratorProp;
import csg.data.AppData;

/**
 *
 * @author Aaron
 */
public class CourseWorkspace  extends AppWorkspaceComponent{
    // THIS PROVIDES US WITH ACCESS TO THE APP COMPONENTS

    CourseSiteGeneratorApp app;

    // THIS PROVIDES RESPONSES TO INTERACTIONS WITH THIS WORKSPACE

    jTPS jTPS;
    
    BorderPane Course;

    // NOTE THAT EVERY CONTROL IS PUT IN A BOX TO HELP WITH ALIGNMENT
    // FOR THE HEADER ON THE LEFT
    TilePane courseBox;
    VBox tempBox;
    TilePane pageBox;
    
    CourseController controller;
    
    Label courseLabel;
    Label subLabel;
    Label semLabel;
    Label numLabel;
    Label yearLabel;
    Label titleLabel;
    Label instLabel;
    Label homeLabel;
    Label exportLabel;
    Label exportDir;
    
    Label tempLabel;
    Label tempText;
    
    Label pageLabel;
    Label styleLabel;
    Label bannerLabel;
    Label lfooterLabel;
    Label rfooterLabel;
    Label noteLabel;
    
    Image bannerImage;
    Image lfooterImage;
    Image rfooterImage;
    
    ImageView bannerView;
    ImageView lfooterView;
    ImageView rfooterView;
    
    Button exportButton;
    Button tempButton;
    Button bannerButton;
    Button lfooterButton;
    Button rfooterButton;
    
    ObservableList<String> years;
    ObservableList<String> semesters;
    ObservableList<String> subjects;
    ObservableList<String> numbers;
    ObservableList<String> styles;
    
    TextField subCombo;
    TextField semCombo;
    TextField numCombo;
    TextField yearCombo;
    ComboBox styleCombo;
    
    TextField TitleTextField;
    TextField InstructorTextField;
    TextField HomeTextField;
    
    TableView PageGrid;
    HashMap<String, Pane> PageGridHeaderPanes;
    HashMap<String, Label> PageGridHeaderLabels;
    HashMap<String, Pane> PageGridCellPanes;
    HashMap<String, Label> PageGridCellLabels;
    

    public CourseWorkspace(CourseSiteGeneratorApp initApp){
        app = initApp;
        jTPS = new jTPS();
        controller = new CourseController(app);
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        courseBox = new TilePane();
        courseBox.setMaxWidth(650);
        courseBox.setPrefColumns(4);
        courseBox.setVgap(10);
        courseBox.setHgap(30);

        subCombo = new TextField();
        semCombo= new TextField();
        numCombo= new TextField();
        yearCombo = new TextField();
        
        
        courseLabel = new Label(props.getProperty(CourseSiteGeneratorProp.COURSE_TEXT.toString()));
        subLabel = new Label(props.getProperty(CourseSiteGeneratorProp.SUBJECT_TEXT.toString()));
        semLabel = new Label(props.getProperty(CourseSiteGeneratorProp.SEMESTER_TEXT.toString()));
        numLabel = new Label(props.getProperty(CourseSiteGeneratorProp.NUMBER_TEXT.toString()));
        yearLabel = new Label(props.getProperty(CourseSiteGeneratorProp.YEAR_TEXT.toString()));
        titleLabel = new Label(props.getProperty(CourseSiteGeneratorProp.TITLE_TEXT.toString()));
        instLabel = new Label(props.getProperty(CourseSiteGeneratorProp.INSTRUCTOR_TEXT.toString()));
        homeLabel = new Label(props.getProperty(CourseSiteGeneratorProp.HOME_TEXT.toString()));
        exportLabel = new Label(props.getProperty(CourseSiteGeneratorProp.EXPORT_TEXT.toString()));
        exportDir = new Label("");
        exportDir.setMaxWidth(300);
        
        exportButton = new Button(props.getProperty(CourseSiteGeneratorProp.CHANGE_BUTTON_TEXT.toString()));
        
        TitleTextField = new TextField();
        InstructorTextField = new TextField();
        HomeTextField = new TextField();
        
        VBox blah = new VBox();
        blah.setAlignment(Pos.CENTER);
        blah.getChildren().add(courseLabel);
        blah.setStyle("-fx-background-color: #ccccff; -fx-border-color:black");
        courseBox.getChildren().add(new Label(" "));
        courseBox.getChildren().add(new Label(" "));
        courseBox.getChildren().add(new Label(" "));
        courseBox.getChildren().add(new Label(" "));
        
        courseBox.getChildren().add(subLabel);
        courseBox.getChildren().add(subCombo);
        
        courseBox.getChildren().add(semLabel);
        courseBox.getChildren().add(semCombo);
        
        courseBox.getChildren().add(numLabel);
        courseBox.getChildren().add(numCombo);
        
        courseBox.getChildren().add(yearLabel);
        courseBox.getChildren().add(yearCombo);
        
        TilePane bleh = new TilePane();
        bleh.setPrefColumns(2);
        bleh.setMaxWidth(700);
        bleh.setVgap(10);
        bleh.getChildren().add(new Label("               "));
        bleh.getChildren().add(new Label("               "));
        bleh.getChildren().add(titleLabel);
        bleh.getChildren().add(TitleTextField);
        bleh.getChildren().add(instLabel);
        bleh.getChildren().add(InstructorTextField);
        bleh.getChildren().add(homeLabel);
        bleh.getChildren().add(HomeTextField);
        bleh.getChildren().add(exportLabel);
        bleh.getChildren().add(exportButton);
        bleh.getChildren().add(new Label("   "));
        bleh.getChildren().add(exportDir);
        
        tempBox = new VBox();
        tempLabel = new Label(props.getProperty(CourseSiteGeneratorProp.TEMPLATE_TEXT.toString()));
        tempText = new Label(props.getProperty(CourseSiteGeneratorProp.TEMPLATE_DESCRIPTION_TEXT.toString()));
        tempButton = new Button(props.getProperty(CourseSiteGeneratorProp.TEMPLATE_BUTTON_TEXT.toString()));
        
        PageGrid = new TableView();
        CourseData data = ((AppData)app.getDataComponent()).getCourseData();
        PageGrid.setMinHeight(200);
        PageGrid.setItems(data.getTemplate());
        TableColumn UseCol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.COURSE_USE_COL.toString()));
        UseCol.setCellValueFactory(new PropertyValueFactory<CourseTemp,Boolean>("Use"));
        UseCol.setCellFactory(CheckBoxTableCell.forTableColumn(UseCol));
        
        TableColumn NavbarCol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.COURSE_NAV_COL.toString()));
        NavbarCol.setCellValueFactory(new PropertyValueFactory<CourseTemp,String>("Navbar"));
        
        TableColumn FileCol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.COURSE_FILE_COL.toString()));
        FileCol.setCellValueFactory(new PropertyValueFactory<CourseTemp,String>("Name"));
        
        TableColumn ScriptCol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.COURSE_SCRIPT_COL.toString()));
        ScriptCol.setCellValueFactory(new PropertyValueFactory<CourseTemp, String>("Script"));
        
        PageGrid.getColumns().addAll(UseCol, NavbarCol, FileCol, ScriptCol);

        VBox head = new VBox();
        head.setAlignment(Pos.CENTER);
        head.getChildren().add(tempLabel);
        head.setStyle("-fx-background-color: #ccccff; -fx-border-color:black");
        tempBox.getChildren().add(head);
        tempBox.getChildren().add(new Label("      "));
        tempBox.getChildren().add(tempText);
        tempBox.getChildren().add(new Label("      "));
        tempBox.getChildren().add(tempButton);
        tempBox.getChildren().add(new Label("      "));
        tempBox.getChildren().add(PageGrid);
        
        pageBox = new TilePane();
        pageBox.setMaxWidth(800);
        pageBox.setAlignment(Pos.CENTER);
        pageBox.setPrefColumns(3);
        pageBox.setHgap(60);
        pageBox.setVgap(5);
        pageLabel = new Label(props.getProperty(CourseSiteGeneratorProp.PAGE_TEXT.toString()));
        bannerLabel = new Label(props.getProperty(CourseSiteGeneratorProp.BANNER_TEXT.toString()));
        lfooterLabel = new Label(props.getProperty(CourseSiteGeneratorProp.LFOOTER_TEXT.toString()));
        rfooterLabel = new Label(props.getProperty(CourseSiteGeneratorProp.RFOOTER_TEXT.toString()));
        styleLabel = new Label(props.getProperty(CourseSiteGeneratorProp.STYLE_TEXT.toString()));
        noteLabel = new Label(props.getProperty(CourseSiteGeneratorProp.STYLE_NOTE_TEXT.toString()));
        
        bannerButton = new Button(props.getProperty(CourseSiteGeneratorProp.CHANGE_BUTTON_TEXT.toString()));
        lfooterButton = new Button(props.getProperty(CourseSiteGeneratorProp.CHANGE_BUTTON_TEXT.toString()));
        rfooterButton = new Button(props.getProperty(CourseSiteGeneratorProp.CHANGE_BUTTON_TEXT.toString()));
        styleCombo = new ComboBox();
        styleCombo.setItems(data.getCSS());
        styleCombo.setMaxWidth(200);
        
        bannerImage = new Image("/", true);
        rfooterImage = new Image("/", true);
        lfooterImage = new Image("/", true);
        
        bannerView = new ImageView(bannerImage);
        rfooterView = new ImageView(rfooterImage);
        lfooterView = new ImageView(lfooterImage);
        
        pageBox.getChildren().add(bannerLabel);
        pageBox.getChildren().add(bannerView);
        pageBox.getChildren().add(bannerButton);
        
        pageBox.getChildren().add(lfooterLabel);
        pageBox.getChildren().add(lfooterView);
        pageBox.getChildren().add(lfooterButton);
        
        pageBox.getChildren().add(rfooterLabel);
        pageBox.getChildren().add(rfooterView);
        pageBox.getChildren().add(rfooterButton);
        
        pageBox.getChildren().add(styleLabel);
        pageBox.getChildren().add(styleCombo);
        pageBox.getChildren().add(new Label(" "));
        //pageBox.getChildren().add(noteLabel);
        
        Course = new BorderPane();
        VBox holder = new VBox();
        holder.setAlignment(Pos.CENTER);
        VBox temp = new VBox();
        temp.setAlignment(Pos.CENTER);
        temp.setMaxWidth(1200);
        temp.getChildren().addAll(blah, courseBox, bleh);
        temp.setStyle("-fx-border-color: black");
        //Course.setTop(temp);
        tempBox.setStyle("-fx-border-color: black");
        tempBox.setAlignment(Pos.CENTER);
        tempBox.setMaxWidth(1200);
        //Course.setCenter(tempBox);
        VBox atemp = new VBox();
        atemp.setMaxWidth(1200);
        atemp.setAlignment(Pos.CENTER);
        atemp.getChildren().add(pageBox);
        atemp.getChildren().add(noteLabel);
        atemp.setStyle("-fx-border-color: black");
        //Course.setBottom(atemp);
        tempButton.setOnAction(e -> {
            controller.handleChangeTemplate();
        });
        exportButton.setOnAction(e -> {
            controller.handleChangeDirectory();
        });
        PageGrid.setOnMouseClicked(e -> {
            controller.handleCheckBox();
        });
        bannerButton.setOnAction(e ->{
            controller.handleChangeBanner();
        });
        lfooterButton.setOnAction(e ->{
            controller.handleChangeLFooter();
        });
        rfooterButton.setOnAction(e -> {
            controller.handleChangeRFooter();
        });
        TitleTextField.textProperty().addListener((observable, oldValue, newValue)->{
            controller.handleChangeInfo();
        });
        subCombo.textProperty().addListener((observable, oldValue, newValue) -> {
            controller.handleChangeInfo();
        });
        semCombo.textProperty().addListener((observable, oldValue, newValue) -> {
            controller.handleChangeInfo();
        });
        numCombo.textProperty().addListener((observable, oldValue, newValue) -> {
            controller.handleChangeInfo();
        });
        yearCombo.textProperty().addListener((observable, oldValue, newValue) -> {
            controller.handleChangeInfo();
        });
        InstructorTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            controller.handleChangeInfo();
        });
        HomeTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            controller.handleChangeInfo();
        });
        styleCombo.setOnAction(e ->{
            controller.handleChangeCSS();
        });
        holder.getChildren().addAll(temp, tempBox, atemp);
        Course.setCenter(holder);
        
    }
    public BorderPane getCourse(){
        return Course;
    }
    public Label getcourseLabel(){
        return courseLabel;
    }
    public Label getsubLabel() {
        return subLabel;
    }
    public Label getsemLabel() {
        return semLabel;
    }
    public Label getnumLabel() {
        return numLabel;
    }
    public Label getyearLabel() {
        return yearLabel;
    }
    public Label gettitleLabel() {
        return titleLabel;
    }
    public Label getinstLabel() {
        return instLabel;
    }
    public Label gethomeLabel() {
        return homeLabel;
    }
    public Label getexportLabel() {
        return exportLabel;
    }
    public Label gettempLabel() {
        return tempLabel;
    }
    public Label gettempText() {
        return tempLabel;
    }
    public Label getpageLabel() {
        return pageLabel;
    }
    public Label getstyleLabel() {
        return styleLabel;
    }
    public Label getbannerLabel() {
        return bannerLabel;
    }
    public Label getlfooterLabel() {
        return lfooterLabel;
    }
    public Label getrfooterLabel() {
        return rfooterLabel;
    }
    public Label getnoteLabel() {
        return noteLabel;
    }
    public Button getexportButton(){
        return exportButton;
    }
    public Button gettempButton(){
        return tempButton;
    }
    public Button getbannerButton(){
       return bannerButton;
    }
    public Button getlfooterButton(){
       return lfooterButton;
    }
    public Button getrfooterButton(){
       return rfooterButton;
    }
    public TextField getsubCombo(){
        return subCombo;
    }
    public TextField getsemCombo() {
        return semCombo;
    }
    public TextField getnumCombo() {
        return numCombo;
    }
    public TextField getyearCombo() {
        return yearCombo;
    }
    public ComboBox getstyleCombo() {
        return styleCombo;
    }
    public TextField getTitleTextField(){
        return TitleTextField;
    }
    public TextField getInstructorTextField(){
        return InstructorTextField;
    }
    public TextField getHomeTextField(){
        return HomeTextField;
    }
    public ImageView getBannerView(){
        return bannerView;
    }
    public ImageView getLFooterView(){
        return lfooterView;
    }
    public ImageView getRFooterView(){
        return rfooterView;
    }
    public jTPS getjTPS(){
        return jTPS;
    }
    public void setjTPS(jTPS j){
        jTPS = j;
    }
    public Label getExportDir(){
        return exportDir;
    }
     public TableView getPageGrid(){
        return PageGrid;
    }
    @Override
    public void resetWorkspace() {
        getPageGrid().getColumns().clear();
        getBannerView().setImage(null);
        getLFooterView().setImage(null);
        getRFooterView().setImage(null);
        setjTPS(new jTPS());
        subCombo.setText("");
        numCombo.setText("");
        semCombo.setText("");
        yearCombo.setText("");
        TitleTextField.setText("");
        InstructorTextField.setText("");
        HomeTextField.setText("");
    }

    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        CourseData data = (CourseData)dataComponent;
        getPageGrid().setItems(data.getTemplate());
        TableColumn UseCol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.COURSE_USE_COL.toString()));
        UseCol.setCellValueFactory(new PropertyValueFactory<CourseTemp, Boolean>("Use"));
        UseCol.setCellFactory(CheckBoxTableCell.forTableColumn(UseCol));

        TableColumn NavbarCol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.COURSE_NAV_COL.toString()));
        NavbarCol.setCellValueFactory(new PropertyValueFactory<CourseTemp, String>("Navbar"));

        TableColumn FileCol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.COURSE_FILE_COL.toString()));
        FileCol.setCellValueFactory(new PropertyValueFactory<CourseTemp, String>("Name"));

        TableColumn ScriptCol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.COURSE_SCRIPT_COL.toString()));
        ScriptCol.setCellValueFactory(new PropertyValueFactory<CourseTemp, String>("Script"));

        getPageGrid().getColumns().addAll(UseCol, NavbarCol, FileCol, ScriptCol);
        String course = data.getCourse();
        String number = data.getNumber();
        String semester = data.getSemester();
        String year = data.getYear();
        String title = data.getTitle();
        String instructor = data.getInstr();
        String home = data.getHome();
        subCombo.setText(home);
        numCombo.setText(number);
        semCombo.setText(semester);
        yearCombo.setText(year);
        TitleTextField.setText(title);
        InstructorTextField.setText(instructor);
        HomeTextField.setText(home);
    }
}
