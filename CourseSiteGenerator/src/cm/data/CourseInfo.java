/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cm.data;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
/**
 *
 * @author Aaron
 */
public class CourseInfo {
    private final StringProperty course;
    private final StringProperty number;
    private final StringProperty semester;
    private final StringProperty title;
    private final StringProperty year;
    private final StringProperty instructor;
    private final StringProperty home;
    
    public CourseInfo(String c, String n, String s, String t, String y, String i, String h){
        course = new SimpleStringProperty(c);
        number = new SimpleStringProperty(n);
        semester = new SimpleStringProperty(s);
        title = new SimpleStringProperty(t);
        year = new SimpleStringProperty(y);
        instructor = new SimpleStringProperty(i);
        home = new SimpleStringProperty(h);
    }
    public String getCourse(){
        return course.get();
    }
    public void setCourse(String s){
        course.set(s);
    }
    public String getNumber(){
        return number.get();
    } 
    public void setNumber(String s){
        number.set(s);
    }
    public String getSemester(){
        return semester.get();
    }
    public void setSemester(String s){
        semester.set(s);
    }
    public String getTitle(){
        return title.get();
    }
    public void setTitle(String s){
        title.set(s);
    }
    public String getYear(){
        return year.get();
    }
    public void setYear(String s){
        year.set(s);
    }
    public String getInstr(){
        return instructor.get();
    }
    public void setInstr(String s){
        instructor.set(s);
    }
    public String getHome(){
        return home.get();
    }
    public void setHome(String s){
        home.set(s);
    }
}
