/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cm.data;

import djf.components.AppDataComponent;
import java.io.File;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import tam.CourseSiteGeneratorApp;

/**
 *
 * @author Aaron
 */
public class CourseData implements AppDataComponent {
    CourseSiteGeneratorApp app;
    CourseInfo info;
    ObservableList<CourseTemp> template;
    ObservableList<String> css;
    File sourcefolder;
    Image banner;
    Image lfooter;
    Image rfooter;
    File export;
    File mycss;
    public CourseData(CourseSiteGeneratorApp initapp){
        app = initapp;
        info = new CourseInfo("","","","", "", "", "");
        template = FXCollections.observableArrayList();
        css = FXCollections.observableArrayList();
        banner = null;
        lfooter = null;
        rfooter = null;
        export = null;
        sourcefolder = null;
        mycss = null;
    }
    
    public void setCourseInfo(String c, String n, String s, String t, String y, String i, String h){
        info.setCourse(c);
        info.setNumber(n);
        info.setSemester(s);
        info.setTitle(t);
        info.setYear(y);
        info.setInstr(i);
        info.setHome(s);
    }   
    
    public void addTemp(String nav, String name, String script, Boolean use){
        template.add(new CourseTemp(nav, name, script, use));
    }
    public ObservableList getTemplate(){
    return template;
    }
    public ObservableList getCSS(){
        return css;
    }
    public void addCSS(String s){
        css.add(s);
    }
    public CourseInfo getCourseInfo(){
        return info;
    }
    public String getCourse(){
        return info.getCourse();
    }
    public String getNumber(){
        return info.getNumber();
    }
    public String getSemester(){
        return info.getSemester();
    }
    public String getTitle(){
        return info.getTitle();
    }
    public String getYear(){
        return info.getYear();
    }
    public String getInstr(){
        return info.getInstr();
    }
    public String getHome(){
        return info.getHome();
    }
    public void setBanner(Image file){
        banner = file;
    }
    public void setLFooter(Image file){
        lfooter = file;
    }
    public void setRFooter(Image file){
        rfooter = file;
    }
    public void setExport(File file){
        export = file;
    }
    public void setSourceFolder(File file){
        sourcefolder = file;
    }
    public Image getBanner(){
        return banner;
    }
    public Image getLFooter(){
        return lfooter;
    }
    public Image getRFooter(){
        return rfooter;
    }
    public File getExport(){
        return export;
    }
    public File getSourceFolder(){
        return sourcefolder;
    }
    public File getmyCSS(){
        return mycss;
    }
    public void setmyCSS(File file){
        mycss = file;
    }
    @Override
    public void resetData() {
        info = new CourseInfo("","","","","", "", "");
        template.clear();
        css.clear();
        banner = null;
        lfooter = null;
        rfooter = null;
        export = null;
        sourcefolder = null;
        mycss = null;
    }
    
}
