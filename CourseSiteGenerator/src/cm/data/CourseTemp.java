/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cm.data;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author caaro
 */
public class CourseTemp {
    private final StringProperty navbar;
    private final StringProperty name;
    private final StringProperty script;
    private final BooleanProperty use;
    public CourseTemp(String nav, String n, String s, Boolean u){
        navbar = new SimpleStringProperty(nav);
        name = new SimpleStringProperty(n);
        script = new SimpleStringProperty(s);
        use = new SimpleBooleanProperty(u);
    }
    public String getNavbar(){
        return navbar.get();
    }
    public void setNavbar(String s){
        navbar.set(s);
    }
    public String getName(){
        return name.get();
    }
    public void setName(String s){
        name.set(s);
    }
    public String getScript(){
        return script.get();
    }
    public void setScript(String s){
        script.set(s);
    }
    public BooleanProperty UseProperty(){
        return use;
    }
    public boolean getUse(){
        return use.get();
    }
    public void setUse(Boolean t){
        use.set(t);
    }
}
