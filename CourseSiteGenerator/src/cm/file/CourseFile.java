/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cm.file;
import cm.data.CourseData;
import csg.data.AppData;
import csg.workspace.AppWorkspace;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javafx.collections.ObservableList;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import tam.CourseSiteGeneratorApp;
/**
 *
 * @author Aaron
 */
public class CourseFile implements AppFileComponent{
    CourseSiteGeneratorApp app;
    
    JsonObject filedata;
    static final String JSON_COURSE = "course";
    static final String JSON_NUMBER = "number";
    static final String JSON_SEMESTER = "semester";
    static final String JSON_YEAR = "year";
    static final String JSON_TITLE = "title";
    static final String JSON_INSTRUCTOR = "instructor";
    static final String JSON_HOME = "home";
    public CourseFile(CourseSiteGeneratorApp initApp){
        app = initApp;
    }
    
    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        CourseData dataManager = (CourseData)data;
        
        JsonObject courseInfo = Json.createObjectBuilder()
                .add(JSON_COURSE, dataManager.getCourse())
                .add(JSON_NUMBER, dataManager.getNumber())
                .add(JSON_SEMESTER, dataManager.getSemester())
                .add(JSON_YEAR, dataManager.getYear())
                .add(JSON_TITLE, dataManager.getTitle())
                .add(JSON_INSTRUCTOR, dataManager.getInstr())
                .add(JSON_HOME, dataManager.getHome())
                .build();
        
        filedata = courseInfo;
    }

    public JsonObject getFileData(){
        return filedata;
    }
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        CourseData dataManager = (CourseData) data;
        JsonObject json = loadJSONFile(filePath);
        String course = json.getString(JSON_COURSE);
        String number = json.getString(JSON_NUMBER);
        String semester = json.getString(JSON_SEMESTER);
        String year = json.getString(JSON_YEAR);
        String title = json.getString(JSON_TITLE);
        String instr = json.getString(JSON_INSTRUCTOR);
        String home = json.getString(JSON_HOME);
        dataManager.setCourseInfo(course, number, semester, title, year, instr, home);
        ((AppWorkspace) app.getWorkspaceComponent()).getCourseWorkspace().reloadWorkspace(dataManager);
        
    }

    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject().getJsonObject("Course");
        jsonReader.close();
        is.close();
        return json;
    }
    
    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        CourseData dataManager = (CourseData) data;

        JsonObject courseInfo = Json.createObjectBuilder()
                .add(JSON_COURSE, dataManager.getCourse())
                .add(JSON_NUMBER, dataManager.getNumber())
                .add(JSON_SEMESTER, dataManager.getSemester())
                .add(JSON_YEAR, dataManager.getYear())
                .add(JSON_TITLE, dataManager.getTitle())
                .add(JSON_INSTRUCTOR, dataManager.getInstr())
                .add(JSON_HOME, dataManager.getHome())
                .build();

        
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(courseInfo);
        jsonWriter.close();

        // INIT THE WRITER
        OutputStream os = new FileOutputStream(filePath, false);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(courseInfo);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();
    }

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
