/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;
import csg.data.AppData;
import jtps.jTPS_Transaction;
import tam.data.TeachingAssistant;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import java.util.ArrayList;
import javafx.beans.property.StringProperty;
import tam.CourseSiteGeneratorApp;
import tam.data.TAData;

/**
 *
 * @author Aaron
 */
public class TA_Delete_Transaction implements jTPS_Transaction {
    CourseSiteGeneratorApp app;
    private String taName;
    private String taEmail;
    ArrayList<TA_Transaction> ListTA;
    public TA_Delete_Transaction(CourseSiteGeneratorApp a, String name, String email, ArrayList<TA_Transaction> list){
        app = a;
        taName = name;
        taEmail = email;
        ListTA = list;
    }
    
    @Override
    public void doTransaction(){
         TAData data = ((AppData)app.getDataComponent()).getTAData();
         data.removeTA(taName);
         for (TA_Transaction ta: ListTA){
             ta.doTransaction();
         }
    }
    
    public void undoTransaction(){
         TAData data = ((AppData)app.getDataComponent()).getTAData();
         data.addTA(taName, taEmail);
         for (TA_Transaction ta: ListTA){
             ta.undoTransaction();
         }
    }
}
