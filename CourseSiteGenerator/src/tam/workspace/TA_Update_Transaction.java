/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;
import jtps.jTPS_Transaction;
import tam.data.TeachingAssistant;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import java.util.ArrayList;
import javafx.beans.property.StringProperty;
import tam.CourseSiteGeneratorApp;
import tam.data.TAData;
/**
 *
 * @author Aaron
 */
public class TA_Update_Transaction implements jTPS_Transaction{
    CourseSiteGeneratorApp app;
    TA_Delete_Transaction remove;
    TA_Delete_Transaction update;
    
    public TA_Update_Transaction(CourseSiteGeneratorApp a, TA_Delete_Transaction r, TA_Delete_Transaction u){
        app = a;
        remove = r;
        update = u;
    }
    
    @Override
    public void doTransaction(){
        remove.doTransaction();
        update.undoTransaction();
    }
    
    @Override
    public void undoTransaction(){
        update.doTransaction();
        remove.undoTransaction();
    }
}
