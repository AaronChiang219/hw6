/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;
import csg.data.AppData;
import jtps.jTPS_Transaction;
import tam.data.TeachingAssistant;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import java.util.ArrayList;
import javafx.beans.property.StringProperty;
import tam.CourseSiteGeneratorApp;
import tam.data.TAData;
/**
 *
 * @author Aaron
 */
public class TA_Add_Transaction implements jTPS_Transaction {
    CourseSiteGeneratorApp app;
    private String taName;
    private String taEmail;
    public TA_Add_Transaction(CourseSiteGeneratorApp a, String name, String email){
        app = a;
        taName = name;
        taEmail = email;
    }
    
    @Override
    public void doTransaction(){
        TAData data = ((AppData)app.getDataComponent()).getTAData();
        data.addTA(taName, taEmail);
    }
    
    @Override
    public void undoTransaction(){
        TAData data = ((AppData)app.getDataComponent()).getTAData();
        data.removeTA(taName);
    }
}
