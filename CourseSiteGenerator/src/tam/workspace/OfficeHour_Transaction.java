package tam.workspace;
import csg.data.AppData;
import jtps.jTPS_Transaction;
import tam.data.TeachingAssistant;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import java.util.ArrayList;
import javafx.beans.property.StringProperty;
import tam.CourseSiteGeneratorApp;
import tam.data.TAData;
import tam.file.TimeSlot;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Aaron
 */
public class OfficeHour_Transaction implements jTPS_Transaction{
    CourseSiteGeneratorApp app;
    ArrayList<TimeSlot> prev;
    ArrayList<TimeSlot> current;
    int startHour;
    int endHour;
    int prevStart;
    int prevEnd;
    public OfficeHour_Transaction(CourseSiteGeneratorApp a, ArrayList<TimeSlot> p, ArrayList<TimeSlot> c, int start, int end, int prevs, int preve){
        app = a;
        prev = p;
        current = c;
        startHour = start;
        endHour = end;
        prevStart = prevs;
        prevEnd = preve;
    }
    
    public void doTransaction(){
        TAData data = ((AppData)app.getDataComponent()).getTAData();
        data.initHours(""+startHour, ""+endHour);
        for (TimeSlot ts : current) {
            String day = ts.getDay();
            String time = ts.getTime();
            String name = ts.getName();
            data.addOfficeHoursReservation(day, time, name);
        }
    }
    
    public void undoTransaction(){
        TAData data = ((AppData)app.getDataComponent()).getTAData();
        data.initHours(""+prevStart, ""+prevEnd);
        for (TimeSlot ts: prev){
            data.addOfficeHoursReservation(ts.getDay(), ts.getTime(), ts.getName());
        }
    }
}
