package tam.workspace;

import csg.data.AppData;
import csg.workspace.AppWorkspace;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.scene.control.Button;
import djf.controller.AppFileController;
import djf.ui.AppGUI;
import static tam.CourseSiteGeneratorProp.*;
import djf.ui.AppMessageDialogSingleton;
import djf.ui.AppYesNoCancelDialogSingleton;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import jtps.jTPS;
import jtps.jTPS_Transaction;
import properties_manager.PropertiesManager;
import tam.CourseSiteGeneratorApp;
import tam.CourseSiteGeneratorProp;
import tam.data.TAData;
import tam.data.TeachingAssistant;
import tam.file.TimeSlot;
import tam.style.TAStyle;
import static tam.style.TAStyle.CLASS_HIGHLIGHTED_GRID_CELL;
import static tam.style.TAStyle.CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN;
import static tam.style.TAStyle.CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE;
import tam.workspace.TAWorkspace;

/**
 * This class provides responses to all workspace interactions, meaning
 * interactions with the application controls not including the file
 * toolbar.
 * 
 * @author Richard McKenna
 * @version 1.0
 */
public class TAController {
    // THE APP PROVIDES ACCESS TO OTHER COMPONENTS AS NEEDED
    CourseSiteGeneratorApp app;

    /**
     * Constructor, note that the app must already be constructed.
     */
    public TAController(CourseSiteGeneratorApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
    }
    
    /**
     * This helper method should be called every time an edit happens.
     */    
    private void markWorkAsEdited() {
        // MARK WORK AS EDITED
        AppGUI gui = app.getGUI();
        gui.getFileController().markAsEdited(gui);
    }
    
    /**
     * This method responds to when the user requests to add
     * a new TA via the UI. Note that it must first do some
     * validation to make sure a unique name and email address
     * has been provided.
     */
    public void handleAddTA() {
        // WE'LL NEED THE WORKSPACE TO RETRIEVE THE USER INPUT VALUES
        TAWorkspace workspace = ((AppWorkspace)app.getWorkspaceComponent()).getTAWorkspace();
        TextField nameTextField = workspace.getNameTextField();
        TextField emailTextField = workspace.getEmailTextField();
        String name = nameTextField.getText();
        String email = emailTextField.getText();
        
        //We also need a regular expression to check for a valid email
        Pattern pattern;
        Matcher matcher;
        pattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"+"[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        matcher = pattern.matcher(email);
        
        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TAData data = ((AppData)app.getDataComponent()).getTAData();
        
        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        if (!matcher.matches()){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_TA_EMAIL_TITLE), props.getProperty(MISSING_TA_EMAIL_MESSAGE));
        }
        // DID THE USER NEGLECT TO PROVIDE A TA NAME?
        else if (name.isEmpty()) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_TA_NAME_TITLE), props.getProperty(MISSING_TA_NAME_MESSAGE));            
        }
        // DID THE USER NEGLECT TO PROVIDE A TA EMAIL?
        else if (email.isEmpty()) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(MISSING_TA_EMAIL_TITLE), props.getProperty(MISSING_TA_EMAIL_MESSAGE));                        
        }
        // DOES A TA ALREADY HAVE THE SAME NAME OR EMAIL?
        else if (data.containsTA(name, email)) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE), props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE));                                    
        }
        // EVERYTHING IS FINE, ADD A NEW TA
        else {
            // ADD THE NEW TA TO THE DATA
            jTPS_Transaction add = new TA_Add_Transaction(app,name,email);
            jTPS jTPS = workspace.getjTPS();
            jTPS.addTransaction(add);
            //data.addTA(name, email);
            
            // CLEAR THE TEXT FIELDS
            nameTextField.setText("");
            emailTextField.setText("");
            
            // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
            nameTextField.requestFocus();
            
            // WE'VE CHANGED STUFF
            markWorkAsEdited();
        }
    }

    /**
     * This function provides a response for when the user presses a
     * keyboard key. Note that we're only responding to Delete, to remove
     * a TA.
     * 
     * @param code The keyboard code pressed.
     */
    public void handleKeyPress(KeyCode code) {
        // DID THE USER PRESS THE DELETE KEY?
        if (code == KeyCode.DELETE) {
            // GET THE TABLE
            TAWorkspace workspace =  ((AppWorkspace)app.getWorkspaceComponent()).getTAWorkspace();
            TableView taTable = workspace.getTATable();
            
            // IS A TA SELECTED IN THE TABLE?
            Object selectedItem = taTable.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                // GET THE TA AND REMOVE IT
                TeachingAssistant ta = (TeachingAssistant)selectedItem;
                String taName = ta.getName();
                String taEmail = ta.getEmail();
                TAData data = ((AppData)app.getDataComponent()).getTAData();
                //data.removeTA(taName);
                ArrayList<TA_Transaction> ListTA = new ArrayList();
                
                // AND BE SURE TO REMOVE ALL THE TA'S OFFICE HOURS
                HashMap<String, Label> labels = workspace.getOfficeHoursGridTACellLabels();
                for (Label label : labels.values()) {
                    if (label.getText().equals(taName)
                    || (label.getText().contains(taName + "\n"))
                    || (label.getText().contains("\n" + taName))) {
                        String cellKey = label.getId();
                        int col = Integer.parseInt(cellKey.substring(0, cellKey.indexOf("_")));
                        int row = Integer.parseInt(cellKey.substring(cellKey.indexOf("_") + 1));
                        ListTA.add(new TA_Transaction(app,taName,col,row));
                        //data.removeTAFromCell(label.textProperty(), taName);
                    }
                }
                jTPS_Transaction delete = new TA_Delete_Transaction(app, taName, taEmail, ListTA);
                jTPS jTPS = workspace.getjTPS();
                jTPS.addTransaction(delete);
                
                // WE'VE CHANGED STUFF
                markWorkAsEdited();
            }
        }
    }
    
    public void handleTAEdit(){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        TAWorkspace workspace =  ((AppWorkspace)app.getWorkspaceComponent()).getTAWorkspace();
        TableView taTable = workspace.getTATable();
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        if (selectedItem == null){
            return;
        }
        TeachingAssistant ta = (TeachingAssistant) selectedItem;
        String taName = ta.getName();
        String taEmail = ta.getEmail();
        Button button = workspace.getAddButton();
        TextField name = workspace.getNameTextField();
        TextField email = workspace.getEmailTextField();
        name.setText(taName);
        email.setText(taEmail);
        button.setText(props.getProperty(CourseSiteGeneratorProp.UPDATE_BUTTON_TEXT.toString()));
        button.setOnAction(e -> {
            updateTA(ta);
        });
    }
    public void updateTA(TeachingAssistant ta){
        TAWorkspace workspace =  ((AppWorkspace)app.getWorkspaceComponent()).getTAWorkspace();
        TextField nameTextField = workspace.getNameTextField();
        TextField emailTextField = workspace.getEmailTextField();
        String taName = ta.getName();
        String taEmail = ta.getEmail();
        String name = nameTextField.getText();
        String email = emailTextField.getText();
        Button button = workspace.getAddButton();

        //We also need a regular expression to check for a valid email
        Pattern pattern;
        Matcher matcher;
        pattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        matcher = pattern.matcher(email);

        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TAData data = ((AppData)app.getDataComponent()).getTAData();
        //data.removeTA(ta.getName());

        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        if (!matcher.matches()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_TA_EMAIL_TITLE), props.getProperty(MISSING_TA_EMAIL_MESSAGE));
        } // DID THE USER NEGLECT TO PROVIDE A TA NAME?
        else if (name.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_TA_NAME_TITLE), props.getProperty(MISSING_TA_NAME_MESSAGE));
        } // DID THE USER NEGLECT TO PROVIDE A TA EMAIL?
        else if (email.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_TA_EMAIL_TITLE), props.getProperty(MISSING_TA_EMAIL_MESSAGE));
        } // DOES A TA ALREADY HAVE THE SAME NAME OR EMAIL?
        else if (taName.equals(name) && taEmail.equals(email)) {
            nameTextField.setText("");
            emailTextField.setText("");

            button.setText(props.getProperty(CourseSiteGeneratorProp.ADD_BUTTON_TEXT.toString()));
            button.setOnAction(e -> {
                handleAddTA();
            });
            // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
            nameTextField.requestFocus();
        } // EVERYTHING IS FINE, ADD A NEW TA
        else {
            // ADD THE NEW TA TO THE DATA
            //data.addTA(name, email);
            //ta.setEmail(email);
            //ta.setName(name);
            ArrayList<TA_Transaction> OListTA = new ArrayList();
            ArrayList<TA_Transaction> NListTA = new ArrayList();
            
            workspace.reloadTATable();
            
            //edits the name if it was changed
            HashMap<String, Label> labels = workspace.getOfficeHoursGridTACellLabels();
            for (Label label : labels.values()) {
                if (label.getText().equals(taName)
                        || (label.getText().contains(taName + "\n"))
                        || (label.getText().contains("\n" + taName))) {
                    String cellKey = label.getId();
                    int col = Integer.parseInt(cellKey.substring(0, cellKey.indexOf("_")));
                    int row = Integer.parseInt(cellKey.substring(cellKey.indexOf("_") + 1));
                    OListTA.add(new TA_Transaction(app, taName, col, row));
                    NListTA.add(new TA_Transaction(app, name, col, row));
                    //label.setText(label.getText().replace(taName, name));
                }
            }
            TA_Delete_Transaction old = new TA_Delete_Transaction(app, taName, taEmail, OListTA);
            TA_Delete_Transaction change = new TA_Delete_Transaction(app, name, email, NListTA);
            jTPS_Transaction update = new TA_Update_Transaction(app, old, change);
            jTPS jTPS = workspace.getjTPS();
            jTPS.addTransaction(update);
            
            // CLEAR THE TEXT FIELDS
            nameTextField.setText("");
            emailTextField.setText("");

            button.setText(props.getProperty(CourseSiteGeneratorProp.ADD_BUTTON_TEXT.toString()));
            button.setOnAction(e -> {
                handleAddTA();
            });
            // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
            nameTextField.requestFocus();

            // WE'VE CHANGED STUFF
            markWorkAsEdited();
        }
    }
    
    public void clearTA(){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        TAWorkspace workspace =  ((AppWorkspace)app.getWorkspaceComponent()).getTAWorkspace();
        TextField name = workspace.getNameTextField();
        TextField email = workspace.getEmailTextField();
        Button button = workspace.getAddButton();
        name.setText("");
        email.setText("");
        name.requestFocus();
        button.setText(props.getProperty(CourseSiteGeneratorProp.ADD_BUTTON_TEXT.toString()));
        button.setOnAction(e -> {
            handleAddTA();
        });
    }
    /**
     * This function provides a response for when the user clicks
     * on the office hours grid to add or remove a TA to a time slot.
     * 
     * @param pane The pane that was toggled.
     */
    public void handleCellToggle(Pane pane) {
        // GET THE TABLE
        TAWorkspace workspace =  ((AppWorkspace)app.getWorkspaceComponent()).getTAWorkspace();
        TableView taTable = workspace.getTATable();
        
        // IS A TA SELECTED IN THE TABLE?
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            // GET THE TA
            TeachingAssistant ta = (TeachingAssistant)selectedItem;
            String taName = ta.getName();
            TAData data = ((AppData)app.getDataComponent()).getTAData();
            String cellKey = pane.getId();
            int col = Integer.parseInt(cellKey.substring(0,cellKey.indexOf("_")));
            int row = Integer.parseInt(cellKey.substring(cellKey.indexOf("_")+1));
            jTPS_Transaction transaction = new TA_Transaction(app, taName, col, row);
            jTPS jTPS = workspace.getjTPS();
            jTPS.addTransaction(transaction);
            
            // AND TOGGLE THE OFFICE HOURS IN THE CLICKED CELL
            //data.toggleTAOfficeHours(cellKey, taName);
            
            // WE'VE CHANGED STUFF
            markWorkAsEdited();
        }
    }
    
   /* void handleGridCellMouseExited(Pane pane) {
        String cellKey = pane.getId();
        TAData data = (TAData)app.getDataComponent();
        int column = Integer.parseInt(cellKey.substring(0, cellKey.indexOf("_")));
        int row = Integer.parseInt(cellKey.substring(cellKey.indexOf("_") + 1));
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();

        Pane mousedOverPane = workspace.getTACellPane(data.getCellKey(column, row));
        mousedOverPane.getStyleClass().clear();
        mousedOverPane.getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);

        // THE MOUSED OVER COLUMN HEADER
        Pane headerPane = workspace.getOfficeHoursGridDayHeaderPanes().get(data.getCellKey(column, 0));
        headerPane.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);

        // THE MOUSED OVER ROW HEADERS
        headerPane = workspace.getOfficeHoursGridTimeCellPanes().get(data.getCellKey(0, row));
        headerPane.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        headerPane = workspace.getOfficeHoursGridTimeCellPanes().get(data.getCellKey(1, row));
        headerPane.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        
        // AND NOW UPDATE ALL THE CELLS IN THE SAME ROW TO THE LEFT
        for (int i = 2; i < column; i++) {
            cellKey = data.getCellKey(i, row);
            Pane cell = workspace.getTACellPane(cellKey);
            cell.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
            cell.getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);
        }

        // AND THE CELLS IN THE SAME COLUMN ABOVE
        for (int i = 1; i < row; i++) {
            cellKey = data.getCellKey(column, i);
            Pane cell = workspace.getTACellPane(cellKey);
            cell.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
            cell.getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);
        }
    }

    void handleGridCellMouseEntered(Pane pane) {
        String cellKey = pane.getId();
        TAData data = (TAData)app.getDataComponent();
        int column = Integer.parseInt(cellKey.substring(0, cellKey.indexOf("_")));
        int row = Integer.parseInt(cellKey.substring(cellKey.indexOf("_") + 1));
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        
        // THE MOUSED OVER PANE
        Pane mousedOverPane = workspace.getTACellPane(data.getCellKey(column, row));
        mousedOverPane.getStyleClass().clear();
        mousedOverPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_CELL);
        
        // THE MOUSED OVER COLUMN HEADER
        Pane headerPane = workspace.getOfficeHoursGridDayHeaderPanes().get(data.getCellKey(column, 0));
        headerPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        
        // THE MOUSED OVER ROW HEADERS
        headerPane = workspace.getOfficeHoursGridTimeCellPanes().get(data.getCellKey(0, row));
        headerPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        headerPane = workspace.getOfficeHoursGridTimeCellPanes().get(data.getCellKey(1, row));
        headerPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        
        // AND NOW UPDATE ALL THE CELLS IN THE SAME ROW TO THE LEFT
        for (int i = 2; i < column; i++) {
            cellKey = data.getCellKey(i, row);
            Pane cell = workspace.getTACellPane(cellKey);
            cell.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        }

        // AND THE CELLS IN THE SAME COLUMN ABOVE
        for (int i = 1; i < row; i++) {
            cellKey = data.getCellKey(column, i);
            Pane cell = workspace.getTACellPane(cellKey);
            cell.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        }
    }*/
    public void handleHoursChange(int startHour, int endHour){
        //where startHour and endHour are military time
        //I will make a new copy of a time slot array and store that, use it for the office hour change transaction
        //one will be old and one will be new copy, outside of that display a message box confirming change
        TAData data = ((AppData)app.getDataComponent()).getTAData();
        TAWorkspace workspace =  ((AppWorkspace)app.getWorkspaceComponent()).getTAWorkspace();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        jTPS jTPS = workspace.getjTPS();
        ArrayList<TimeSlot> officehours = TimeSlot.buildOfficeHoursList(data);
        ArrayList<TimeSlot> nofficehours = TimeSlot.buildOfficeHoursList(data);
        if (startHour<=data.getStartHour() && endHour>=data.getEndHour()){
            //no problems, so expand the office hours grid
            jTPS_Transaction change = new OfficeHour_Transaction(app, officehours, nofficehours, startHour, endHour, data.getStartHour(), data.getEndHour());
            jTPS.addTransaction(change);
            return;
        }
        //check top, check bottom, then if they are to be deleted delete them but add them to some array
        boolean alert = false;
        Iterator<TimeSlot> iterator = nofficehours.iterator();
        while(iterator.hasNext()){
            TimeSlot ts = iterator.next();
            String stime = ts.getTime();
            int time = Integer.parseInt(stime.substring(0,stime.indexOf("_")));
            if (stime.contains("am")){
                if (time==12){
                    time-=12;
                }
            }
            if (stime.contains("pm")){
                if (time!=12){
                    time+=12;
                }
            }
            if (time<startHour || time>endHour){
                alert = true;
                iterator.remove();
            }
        }
        if (alert){
            AppYesNoCancelDialogSingleton yesNoDialog = AppYesNoCancelDialogSingleton.getSingleton();
            yesNoDialog.show(props.getProperty(OVERWRITE_TA_TITLE), props.getProperty(OVERWRITE_TA_MESSAGE));
            String selection = yesNoDialog.getSelection();
            if(selection.equals(AppYesNoCancelDialogSingleton.YES)){
                jTPS_Transaction change = new OfficeHour_Transaction(app, officehours, nofficehours, startHour, endHour, data.getStartHour(), data.getEndHour());
                jTPS.addTransaction(change);
                markWorkAsEdited();
                return;
            } else {
                return;
            }
        } else {
            jTPS_Transaction change = new OfficeHour_Transaction(app, officehours, nofficehours, startHour, endHour, data.getStartHour(), data.getEndHour());
            jTPS.addTransaction(change);
            markWorkAsEdited();
        }
    }
}