package tam.workspace;
import csg.data.AppData;
import jtps.jTPS_Transaction;
import tam.data.TeachingAssistant;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import javafx.beans.property.StringProperty;
import tam.CourseSiteGeneratorApp;
import tam.data.TAData;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Aaron
 */
public class TA_Transaction implements jTPS_Transaction {
    CourseSiteGeneratorApp app;
    private String taName;
    private int col;
    private int row;
    public TA_Transaction(CourseSiteGeneratorApp a, String teacher, int c, int r){
        app = a;
        taName = teacher;
        col = c;
        row = r;
    }
    
    @Override
    public void doTransaction(){
        TAData data = ((AppData)app.getDataComponent()).getTAData();
        String cellKey = data.getCellKey(col, row);
        data.toggleTAOfficeHours(cellKey, taName);
        
    }
    
    @Override
    public void undoTransaction(){
        TAData data = ((AppData)app.getDataComponent()).getTAData();
        String cellKey = data.getCellKey(col, row);
        data.toggleTAOfficeHours(cellKey, taName);
    }
}
