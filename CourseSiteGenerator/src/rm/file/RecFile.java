/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rm.file;

import csg.data.AppData;
import csg.workspace.AppWorkspace;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import javafx.collections.ObservableList;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import rm.data.RecData;
import rm.data.Recitation;
import tam.CourseSiteGeneratorApp;
import tam.data.TAData;

/**
 *
 * @author Aaron
 */
public class RecFile implements AppFileComponent {
    
    CourseSiteGeneratorApp app;
    
    JsonObject filedata;
    static final String JSON_RECITATIONS = "recitations";
    static final String JSON_SECTION = "section";
    static final String JSON_INSTRUCTOR = "instructor";
    static final String JSON_DAYTIME = "day_time";
    static final String JSON_LOCATION = "location";
    static final String JSON_TA1 = "ta_1";
    static final String JSON_TA2 = "ta_2";
    
    
    public RecFile(CourseSiteGeneratorApp initApp) {
        app = initApp;
    }

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        RecData dataManager = (RecData)data;
        
        JsonArrayBuilder recitationArrayBuilder = Json.createArrayBuilder();
        ObservableList<Recitation> recitations = dataManager.getRecitations();
        for (Recitation r: recitations){
            JsonObject recitationJson = Json.createObjectBuilder()
                    .add(JSON_SECTION, r.getSection())
                    .add(JSON_INSTRUCTOR, r.getInstructor())
                    .add(JSON_DAYTIME, r.getDaytime())
                    .add(JSON_LOCATION, r.getLocation())
                    .add(JSON_TA1, r.getTA())
                    .add(JSON_TA2, r.getTA2())
                    .build();
            recitationArrayBuilder.add(recitationJson);
        }
        JsonArray recitationArray = recitationArrayBuilder.build();
        
        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_RECITATIONS, recitationArray)
                .build();
        
        filedata = dataManagerJSO;
    }
    public JsonObject getFileData(){
        return filedata;
    }

    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        RecData dataManager = (RecData)data;
        TAData tadata = ((AppData)app.getDataComponent()).getTAData();
        JsonObject json = loadJSONFile(filePath);
        
        JsonArray jsonRecitationArray = json.getJsonArray(JSON_RECITATIONS);
        for (int i = 0; i < jsonRecitationArray.size(); i++){
            JsonObject jsonRecitation = jsonRecitationArray.getJsonObject(i);
            String section = jsonRecitation.getString(JSON_SECTION);
            String instructor = jsonRecitation.getString(JSON_INSTRUCTOR);
            String daytime = jsonRecitation.getString(JSON_DAYTIME);
            String location = jsonRecitation.getString(JSON_LOCATION);
            String ta1 = jsonRecitation.getString(JSON_TA1);
            String ta2 = jsonRecitation.getString(JSON_TA2);
            dataManager.addRecitation(section, instructor, daytime, location, tadata.getTA(ta1), tadata.getTA(ta2));
        }
        ((AppWorkspace)app.getWorkspaceComponent()).getRecWorkspace().reloadWorkspace(dataManager);
    }
    
    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject().getJsonObject("Rec");
        jsonReader.close();
        is.close();
        return json;
    }

    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        RecData dataManager = (RecData) data;

        JsonArrayBuilder recitationArrayBuilder = Json.createArrayBuilder();
        ObservableList<Recitation> recitations = dataManager.getRecitations();
        for (Recitation r : recitations) {
            JsonObject recitationJson = Json.createObjectBuilder()
                    .add(JSON_SECTION, r.getSection())
                    .add(JSON_INSTRUCTOR, r.getInstructor())
                    .add(JSON_DAYTIME, r.getDaytime())
                    .add(JSON_LOCATION, r.getLocation())
                    .add(JSON_TA1, r.getTA())
                    .add(JSON_TA2, r.getTA2())
                    .build();
            recitationArrayBuilder.add(recitationJson);
        }
        JsonArray recitationArray = recitationArrayBuilder.build();

        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_RECITATIONS, recitationArray)
                .build();

        
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataManagerJSO);
        jsonWriter.close();

        // INIT THE WRITER
        OutputStream os = new FileOutputStream(filePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();
    }

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
