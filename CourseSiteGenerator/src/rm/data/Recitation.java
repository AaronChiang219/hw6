/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rm.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import tam.data.TeachingAssistant;

/**
 *
 * @author Aaron
 */
public class Recitation<E extends Comparable<E>> implements Comparable<E> {
    private final StringProperty section;
    private final StringProperty instructor;
    private final StringProperty location;
    private final StringProperty daytime;
    private TeachingAssistant ta;
    private TeachingAssistant ta2;
    
    public Recitation(String initSection, String initInstructor, String initDaytime, String initLocation, TeachingAssistant initTA, TeachingAssistant initTA2){
        section = new SimpleStringProperty(initSection);
        instructor = new SimpleStringProperty(initInstructor);
        location = new SimpleStringProperty(initLocation);
        daytime = new SimpleStringProperty(initDaytime);
        ta = initTA;
        ta2 = initTA2;
    }
    public String getSection(){
        return section.get();
    }
    public void setSection(String s){
        section.set(s);
    }
    public String getInstructor(){
        return instructor.get();
    }
    public void setInstructor(String s){
        instructor.set(s);
    }
    public String getLocation(){
        return location.get();
    }
    public void setLocation(String s){
        location.set(s);
    }
    public String getDaytime(){
        return daytime.get();
    }
    public void setDaytime(String s){
        daytime.set(s);
    }
    public String getTA(){
        return ta.getName();
    }
    public void setTA(TeachingAssistant s){
        ta = s;
    }
    public String getTA2(){
        return ta2.getName();
    }
    public void setTA2(TeachingAssistant s){
        ta2 = s;
    }
    public TeachingAssistant getTAO(){
        return ta;
    }
    public TeachingAssistant getTA2O(){
        return ta2;
    }
    
    @Override
    public int compareTo(E o) {
        return getSection().compareTo(((Recitation)o).getSection());
    }
    
}
