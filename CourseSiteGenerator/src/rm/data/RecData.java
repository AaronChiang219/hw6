/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rm.data;

import djf.components.AppDataComponent;
import java.util.Collections;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import tam.CourseSiteGeneratorApp;
import tam.data.TeachingAssistant;


/**
 *
 * @author Aaron
 */
public class RecData implements AppDataComponent  {
    CourseSiteGeneratorApp app;
    ObservableList<Recitation> Recitations;
    public RecData(CourseSiteGeneratorApp initApp){
        app = initApp;
        Recitations = FXCollections.observableArrayList();
    }

    @Override
    public void resetData() {
        Recitations.clear();
    }
    
    public ObservableList getRecitations(){
        return Recitations;
    }
    
    public Recitation getRecitation(String section) {
        for (Recitation r : Recitations) {
            if (r.getSection().equals(section)) {
                return r;
            }
        }
        return null;
    }
    public boolean containsRecitation(String section) {
        for (Recitation r : Recitations) {
            if (r.getSection().equals(section)) {
                return true;
            }
        }
        return false;
    }
    
    public void addRecitation(String section, String instructor, String daytime, String location, TeachingAssistant ta, TeachingAssistant ta2){
        Recitation rec = new Recitation(section, instructor, daytime, location, ta, ta2);
        if(!containsRecitation(section)){
            Recitations.add(rec);
        }
        
         Collections.sort(Recitations);
    }
    
    public void removeRecitation(String section){
        for (Recitation r : Recitations) {
            if (section.equals(r.getSection())) {
                Recitations.remove(r);
                return;
            }
        }
    }
    
}
