/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rm.workspace;

import jtps.jTPS_Transaction;
import rm.data.Recitation;
import tam.CourseSiteGeneratorApp;
import tam.data.TeachingAssistant;

/**
 *
 * @author caaro
 */
public class Rec_Edit_Transaction implements jTPS_Transaction{
    CourseSiteGeneratorApp app;
    private String section;
    private String instructor;
    private String location;
    private String daytime;
    private TeachingAssistant ta;
    private TeachingAssistant ta2;
    private Recitation rec;
    private String osection;
    private String oinstructor;
    private String olocation;
    private String odaytime;
    private TeachingAssistant ota;
    private TeachingAssistant ota2;
    public Rec_Edit_Transaction(CourseSiteGeneratorApp a, String s, String i, String l, String d, TeachingAssistant t, TeachingAssistant t2, Recitation r) {
        app = a;
        section = s;
        instructor = i;
        location = l;
        daytime = d;
        ta = t;
        ta2 = t2;
        rec = r;
        osection = rec.getSection();
        oinstructor = rec.getInstructor();
        olocation = rec.getLocation();
        odaytime = rec.getDaytime();
        ota = rec.getTAO();
        ota2 = rec.getTA2O();
        
    }
    @Override
    public void doTransaction() {
       rec.setDaytime(daytime);
       rec.setInstructor(instructor);
       rec.setLocation(location);
       rec.setSection(section);
       rec.setTA(ta);
       rec.setTA2(ta2);
    }

    @Override
    public void undoTransaction() {
        rec.setDaytime(odaytime);
        rec.setInstructor(oinstructor);
        rec.setLocation(olocation);
        rec.setSection(osection);
        rec.setTA(ota);
        rec.setTA2(ota2);
    }
    
}
