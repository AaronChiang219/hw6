/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rm.workspace;

import csg.data.AppData;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import java.util.HashMap;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.StringConverter;
import jtps.jTPS;
import properties_manager.PropertiesManager;
import rm.data.RecData;
import rm.data.Recitation;
import sm.data.Item;
import tam.CourseSiteGeneratorApp;
import tam.CourseSiteGeneratorProp;
import tam.data.TAData;
import tam.data.TeachingAssistant;

/**
 *
 * @author Aaron
 */
public class RecWorkspace extends AppWorkspaceComponent{
    CourseSiteGeneratorApp app;
    jTPS jTPS;
    VBox RecitationBox;
    TilePane EditBox;
    RecController controller;
    
    BorderPane Recitation;
    TextField SectionTextField;
    TextField InstructorTextField;
    TextField TimeTextField;
    TextField LocationTextField;
    
    Label RecLabel;
    Label SectionLabel;
    Label InstructorLabel;
    Label TimeLabel;
    Label LocationLabel;
    Label AddEditLabel;
    
    Label TALabel;
    Label TALabel2;
    
    ComboBox TACombo;
    ComboBox TACombo2;
    
    Button addButton;
    Button clearButton;

    
    TableView RecitationGrid;
    HashMap<String, Pane> RecitationGridHeaderPanes;
    HashMap<String, Label> RecitationGridHeaderLabels;
    HashMap<String, Pane> RecitationGridCellPanes;
    HashMap<String, Label> RecitationGridCellLabels;
    
    public RecWorkspace(CourseSiteGeneratorApp initApp){
        app = initApp;
        jTPS = new jTPS();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        RecitationBox = new VBox();
        RecitationBox.setAlignment(Pos.CENTER);
        
        controller = new RecController(app);
        
        RecData data = ((AppData)app.getDataComponent()).getRecData();
        RecitationGrid = new TableView();
        RecitationGrid.setMaxWidth(1000);
        ObservableList<Recitation> recitationData = data.getRecitations();
        RecitationGrid.setItems(recitationData);
        
        TableColumn SectionCol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.REC_SECTION_COL.toString()));
        SectionCol.setCellValueFactory(new PropertyValueFactory<Recitation,String>("Section"));
        
        TableColumn InstructorCol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.REC_INSTRUCTOR_COL.toString()));
        InstructorCol.setCellValueFactory(new PropertyValueFactory<Recitation, String>("Instructor"));
        
        TableColumn DayCol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.REC_DAY_COL.toString()));
        DayCol.setCellValueFactory(new PropertyValueFactory<Recitation,String>("Daytime"));
        
        TableColumn LocationCol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.REC_LOCATION_COL.toString()));
        LocationCol.setCellValueFactory(new PropertyValueFactory<Recitation, String>("Location"));
        
        TableColumn TACol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.REC_TA_COL.toString()));
        TACol.setCellValueFactory(new PropertyValueFactory<Recitation, String>("TA"));
        
        TableColumn TACol2 = new TableColumn(props.getProperty(CourseSiteGeneratorProp.REC_TA_COL.toString()));
        TACol2.setCellValueFactory(new PropertyValueFactory<Recitation, String>("TA2"));
        
        RecitationGrid.getColumns().addAll(SectionCol, InstructorCol, DayCol, LocationCol, TACol, TACol2);
        RecLabel = new Label(props.getProperty(CourseSiteGeneratorProp.RECITATION_TEXT.toString()));
        VBox blah = new VBox();
        blah.setAlignment(Pos.CENTER);
        blah.getChildren().add(RecLabel);
        blah.setStyle("-fx-background-color: #ccccff; -fx-border-color:black");
        blah.setMaxWidth(1000);
        RecitationBox.getChildren().add(blah);
        RecitationBox.getChildren().add(RecitationGrid);
        //RecitationBox.getChildren().add(new Label("    "));
        
        VBox SomeBox = new VBox();
        SomeBox.setMaxWidth(1000);
        SomeBox.setAlignment(Pos.CENTER);
        SomeBox.setStyle("-fx-border-color: black");
        SomeBox.setMaxHeight(300);
        EditBox = new TilePane();
        EditBox.setPrefColumns(2);
        EditBox.setMaxWidth(700);
        EditBox.setVgap(10);
        AddEditLabel = new Label(props.getProperty(CourseSiteGeneratorProp.ADD_UPDATE_BUTTON_TEXT.toString()));
        SectionLabel = new Label(props.getProperty(CourseSiteGeneratorProp.SECTION_TEXT.toString()));
        InstructorLabel = new Label(props.getProperty(CourseSiteGeneratorProp.INSTRUCTOR_NAME_TEXT.toString()));
        TimeLabel = new Label(props.getProperty(CourseSiteGeneratorProp.TIME_DAY_TEXT.toString()));
        LocationLabel = new Label(props.getProperty(CourseSiteGeneratorProp.LOCATION_TEXT.toString()));
        TALabel = new Label(props.getProperty(CourseSiteGeneratorProp.TA_TEXT.toString()));
        TALabel2 = new Label(props.getProperty(CourseSiteGeneratorProp.TA_TEXT.toString()));
        
        SectionTextField = new TextField();
        InstructorTextField = new TextField();
        TimeTextField = new TextField();
        LocationTextField = new TextField();
        
        TAData tadata = ((AppData)app.getDataComponent()).getTAData();
        TACombo = new ComboBox();
        TACombo2 = new ComboBox();
        Callback back = new Callback<ListView<TeachingAssistant>, ListCell<TeachingAssistant>>(){
            @Override
            public ListCell<TeachingAssistant> call (ListView<TeachingAssistant> ta){
                ListCell cell = new ListCell<TeachingAssistant>(){
                    @Override
                    protected void updateItem(TeachingAssistant item, boolean empty){
                        super.updateItem(item, empty);
                        if (empty){
                            setText("");
                        } else {
                            setText(item.getName());
                        }
                    }
                };
                return cell;
            }
        };
        StringConverter converter = new StringConverter<TeachingAssistant>(){
            @Override
            public String toString(TeachingAssistant object) {
                return object.getName();
            }

            @Override
            public TeachingAssistant fromString(String string) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
            
        };
        TACombo.setItems(tadata.getTeachingAssistants());
        TACombo2.setItems(tadata.getTeachingAssistants());
        TACombo.setCellFactory(back);
        TACombo2.setCellFactory(back);
        TACombo.setConverter(converter);
        TACombo2.setConverter(converter);
        
        addButton = new Button(props.getProperty(CourseSiteGeneratorProp.ADD_UPDATE_BUTTON_TEXT.toString()));
        clearButton = new Button(props.getProperty(CourseSiteGeneratorProp.CLEAR_BUTTON_TEXT.toString()));
        
        VBox bleh = new VBox();
        bleh.setAlignment(Pos.CENTER);
        bleh.getChildren().add(AddEditLabel);
        bleh.setStyle("-fx-background-color: #ccccff; -fx-border-color:black");
        EditBox.getChildren().add(new Label(" "));
        EditBox.getChildren().add(new Label(" "));
        EditBox.getChildren().add(SectionLabel);
        EditBox.getChildren().add(SectionTextField);
        EditBox.getChildren().add(InstructorLabel);
        EditBox.getChildren().add(InstructorTextField);
        EditBox.getChildren().add(TimeLabel);
        EditBox.getChildren().add(TimeTextField);
        EditBox.getChildren().add(LocationLabel);
        EditBox.getChildren().add(LocationTextField);
        EditBox.getChildren().add(TALabel);
        EditBox.getChildren().add(TACombo);
        EditBox.getChildren().add(TALabel2);
        EditBox.getChildren().add(TACombo2);
        EditBox.getChildren().add(addButton);
        EditBox.getChildren().add(clearButton);
        EditBox.getChildren().add(new Label(" "));
        EditBox.getChildren().add(new Label(" "));
        
        SomeBox.getChildren().addAll(bleh, EditBox);
        Recitation = new BorderPane();
        Recitation.setTop(RecitationBox);
        Recitation.setCenter(SomeBox);
        
        addButton.setOnAction(e ->{
            controller.handleAddRecitation();
        });
        clearButton.setOnAction(e -> {
            controller.handleClearRecitation();
        });
        RecitationGrid.setOnMouseClicked(e ->{
            controller.handleRecitationEdit();
        });
        RecitationGrid.setOnKeyPressed(e -> {
            controller.handleKeyPress(e.getCode());
        });
    }
    public BorderPane getRecitation(){
        return Recitation;
    }
    public TextField getSectionTextField(){
        return SectionTextField;
    }
    public TextField getInstructorTextField(){
        return InstructorTextField;
    }
    public TextField getTimeTextField(){
        return TimeTextField;
    }
    public TextField getLocationTextField(){
        return LocationTextField;
    }
    public Label getRecLabel(){
        return RecLabel;
    }
    public Label getSectionLabel(){
        return SectionLabel;
    }
    public Label getInstructorLabel(){
        return InstructorLabel;
    }
    public Label getTimeLabel(){
        return TimeLabel;
    }
    public Label getLocationLabel(){
        return LocationLabel;
    }
    public Label getTALabel(){
        return TALabel;
    }
    public Label getTALabel2(){
        return TALabel2;
    }
    public Label getAddEditLabel(){
        return AddEditLabel;
    }
    public ComboBox getTACombo(){
        return TACombo;
    }
    public ComboBox getTACombo2(){
        return TACombo2;
    }
    public Button getaddButton(){
        return addButton;
    }
    public Button getclearButton(){
        return clearButton;
    }
    public TableView getRecitationGrid(){
        return RecitationGrid;
    }
    public jTPS getjTPS(){
        return jTPS;
    }
    public void setjTPS(jTPS j){
        jTPS = j;
    }
    @Override
    public void resetWorkspace() {
       setjTPS(new jTPS());
       getRecitationGrid().getColumns().clear();
       
    }

    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {
        RecData data = (RecData)dataComponent;
        getRecitationGrid().setItems(data.getRecitations());
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        TableColumn SectionCol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.REC_SECTION_COL.toString()));
        SectionCol.setCellValueFactory(new PropertyValueFactory<Recitation, String>("Section"));

        TableColumn InstructorCol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.REC_INSTRUCTOR_COL.toString()));
        InstructorCol.setCellValueFactory(new PropertyValueFactory<Recitation, String>("Instructor"));

        TableColumn DayCol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.REC_DAY_COL.toString()));
        DayCol.setCellValueFactory(new PropertyValueFactory<Recitation, String>("Daytime"));

        TableColumn LocationCol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.REC_LOCATION_COL.toString()));
        LocationCol.setCellValueFactory(new PropertyValueFactory<Recitation, String>("Location"));

        TableColumn TACol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.REC_TA_COL.toString()));
        TACol.setCellValueFactory(new PropertyValueFactory<Recitation, String>("TA"));

        TableColumn TACol2 = new TableColumn(props.getProperty(CourseSiteGeneratorProp.REC_TA_COL.toString()));
        TACol2.setCellValueFactory(new PropertyValueFactory<Recitation, String>("TA2"));

        getRecitationGrid().getColumns().addAll(SectionCol, InstructorCol, DayCol, LocationCol, TACol, TACol2);
        
        
    }
    
}
