/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rm.workspace;
import csg.data.AppData;
import csg.workspace.AppWorkspace;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.scene.control.Button;
import djf.controller.AppFileController;
import djf.ui.AppGUI;
import static tam.CourseSiteGeneratorProp.*;
import djf.ui.AppMessageDialogSingleton;
import djf.ui.AppYesNoCancelDialogSingleton;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import jtps.jTPS;
import jtps.jTPS_Transaction;
import properties_manager.PropertiesManager;
import rm.data.RecData;
import rm.data.Recitation;
import tam.CourseSiteGeneratorApp;
import tam.CourseSiteGeneratorProp;
import tam.data.TeachingAssistant;
import tam.data.TAData;
/**
 *
 * @author caaro
 */
public class RecController {
    CourseSiteGeneratorApp app;
    
    public RecController(CourseSiteGeneratorApp initApp){
        app = initApp;
    }
    
    private void markWorkAsEdited(){
        AppGUI gui = app.getGUI();
        gui.getFileController().markAsEdited(gui);
    }
    
    public void handleAddRecitation(){
        RecWorkspace workspace = ((AppWorkspace)app.getWorkspaceComponent()).getRecWorkspace();
        TextField sectionTextField = workspace.getSectionTextField();
        TextField instructorTextField = workspace.getInstructorTextField();
        TextField timeTextField = workspace.getTimeTextField();
        TextField locationTextField = workspace.getLocationTextField();
        ComboBox taCombo = workspace.getTACombo();
        ComboBox taCombo2 = workspace.getTACombo2();
        String section = sectionTextField.getText();
        String instructor = instructorTextField.getText();
        String time = timeTextField.getText();
        String location = locationTextField.getText();
        TeachingAssistant ta = (TeachingAssistant) workspace.getTACombo().getValue();
        TeachingAssistant ta2 = (TeachingAssistant) workspace.getTACombo2().getValue();
        RecData data = ((AppData)app.getDataComponent()).getRecData();
        if (data.containsRecitation(section)){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show("Error message to be filled in by xml later", "same");

        } else {
            jTPS_Transaction add = new Rec_Add_Transaction(app, section, instructor, location, time, ta, ta2);
            jTPS jTPS = workspace.getjTPS();
            jTPS.addTransaction(add);
            sectionTextField.setText("");
            instructorTextField.setText("");
            timeTextField.setText("");
            locationTextField.setText("");
            taCombo.setValue("");
            taCombo2.setValue("");
            sectionTextField.requestFocus();
            markWorkAsEdited();
        }
    }
    public void handleKeyPress(KeyCode code){
        RecWorkspace workspace = ((AppWorkspace) app.getWorkspaceComponent()).getRecWorkspace();
        if (code == KeyCode.DELETE){
            TableView recTable = workspace.getRecitationGrid();
            Object selectedItem = recTable.getSelectionModel().getSelectedItem();
            if (selectedItem!=null){
                Recitation rec = (Recitation)selectedItem;
                String section = rec.getSection();
                String instructor = rec.getInstructor();
                String daytime = rec.getDaytime();
                String location = rec.getLocation();
                TeachingAssistant ta = (TeachingAssistant) workspace.getTACombo().getValue();
                TeachingAssistant ta2 = (TeachingAssistant) workspace.getTACombo2().getValue();
                RecData data = ((AppData)app.getDataComponent()).getRecData();
                jTPS_Transaction del = new Rec_Remove_Transaction(app, section, instructor, location, daytime, ta, ta2);
                workspace.getjTPS().addTransaction(del);
                markWorkAsEdited();
            } 
        }
    }
    public void handleRecitationEdit(){
        RecWorkspace workspace = ((AppWorkspace) app.getWorkspaceComponent()).getRecWorkspace();
        TableView recTable = workspace.getRecitationGrid();
        Object selectedItem = recTable.getSelectionModel().getSelectedItem();
        if (selectedItem == null){
            return;
        }
        TAData tadata = ((AppData)app.getDataComponent()).getTAData();
        Recitation rec = (Recitation)selectedItem;
        TextField sectionTextField = workspace.getSectionTextField();
        TextField instructorTextField = workspace.getInstructorTextField();
        TextField daytimeTextField = workspace.getTimeTextField();
        TextField locationTextField = workspace.getLocationTextField();
        ComboBox ta = workspace.getTACombo();
        ComboBox ta2 = workspace.getTACombo2();
        sectionTextField.setText(rec.getSection());
        instructorTextField.setText(rec.getInstructor());
        daytimeTextField.setText(rec.getDaytime());
        locationTextField.setText(rec.getLocation());
        ta.setValue(tadata.getTA(rec.getTA()));
        ta2.setValue(tadata.getTA(rec.getTA2()));
        Button button = workspace.getaddButton();
        button.setOnAction(e -> {
            updateRecitation(rec);
        });
        
    }
    public void updateRecitation(Recitation rec){
        RecWorkspace workspace = ((AppWorkspace) app.getWorkspaceComponent()).getRecWorkspace();
        TextField sectionTextField = workspace.getSectionTextField();
        TextField instructorTextField = workspace.getInstructorTextField();
        TextField daytimeTextField = workspace.getTimeTextField();
        TextField locationTextField = workspace.getLocationTextField();
        TeachingAssistant ta = (TeachingAssistant) workspace.getTACombo().getValue();
        TeachingAssistant ta2 = (TeachingAssistant) workspace.getTACombo2().getValue();
        String section = sectionTextField.getText();
        String instructor = instructorTextField.getText();
        String daytime = daytimeTextField.getText();
        String location = locationTextField.getText();
        
        RecData data = ((AppData)app.getDataComponent()).getRecData();
        if (data.containsRecitation(rec.getSection()) && !section.equals(rec.getSection())){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show("Error message to be filled in by xml later", "same");
        } else {
            jTPS_Transaction edit = new Rec_Edit_Transaction(app, section, instructor, location, daytime, ta, ta2, rec);
            workspace.getjTPS().addTransaction(edit);
            sectionTextField.setText("");
            instructorTextField.setText("");
            daytimeTextField.setText("");
            locationTextField.setText("");
            sectionTextField.requestFocus();
            TableView recTable = workspace.getRecitationGrid();
            ((TableColumn)recTable.getColumns().get(0)).setVisible(false);
            ((TableColumn) recTable.getColumns().get(0)).setVisible(true);
            Button button = workspace.getaddButton();
            button.setOnAction(e -> {
                handleAddRecitation();
            });
            markWorkAsEdited();
        }
    }
    
    public void handleClearRecitation(){
        RecWorkspace workspace = ((AppWorkspace) app.getWorkspaceComponent()).getRecWorkspace();
        TextField sectionTextField = workspace.getSectionTextField();
        TextField instructorTextField = workspace.getInstructorTextField();
        TextField daytimeTextField = workspace.getTimeTextField();
        TextField locationTextField = workspace.getLocationTextField();
        ComboBox ta = workspace.getTACombo();
        ComboBox ta2 = workspace.getTACombo2();
        sectionTextField.setText("");
        instructorTextField.setText("");
        daytimeTextField.setText("");
        locationTextField.setText("");
        ta.setValue("");
        ta2.setValue("");
        sectionTextField.requestFocus();
        Button button = workspace.getaddButton();
        button.setOnAction(e ->{
            handleAddRecitation();
        });
    }
}
