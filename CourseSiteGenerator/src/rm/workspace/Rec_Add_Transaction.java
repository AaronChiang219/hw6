/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rm.workspace;

import csg.data.AppData;
import jtps.jTPS_Transaction;
import rm.data.RecData;
import tam.CourseSiteGeneratorApp;
import tam.data.TeachingAssistant;

/**
 *
 * @author caaro
 */
public class Rec_Add_Transaction implements jTPS_Transaction{
    CourseSiteGeneratorApp app;
    private String section;
    private String instructor;
    private String location;
    private String daytime;
    private TeachingAssistant ta;
    private TeachingAssistant ta2;
    
    public Rec_Add_Transaction(CourseSiteGeneratorApp a, String s, String i, String l, String d, TeachingAssistant t, TeachingAssistant t2){
        app = a;
        section = s;
        instructor = i;
        location = l;
        daytime = d;
        ta = t;
        ta2 = t2;
    }
    @Override
    public void doTransaction() {
        RecData data = ((AppData)app.getDataComponent()).getRecData();
        data.addRecitation(section, instructor, daytime, location, ta, ta2);
    }

    @Override
    public void undoTransaction() {
        RecData data = ((AppData)app.getDataComponent()).getRecData();
        data.removeRecitation(section);
    }
    
}
