/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pm.file;

import csg.workspace.AppWorkspace;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import javafx.collections.ObservableList;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import pm.data.ProjData;
import pm.data.Student;
import pm.data.Team;
import tam.CourseSiteGeneratorApp;

/**
 *
 * @author Aaron
 */
public class ProjFile implements AppFileComponent {
    CourseSiteGeneratorApp app;

    JsonObject filedata;
    static final String JSON_NAME = "name";
    static final String JSON_STUDENTS = "students";
    static final String JSON_LINK = "link";
    static final String JSON_COLOR = "color";
    static final String JSON_TCOLOR = "tcolor";
    static final String JSON_TEAM = "team";
    static final String JSON_TEAMS = "teams";
    static final String JSON_FNAME = "fname";
    static final String JSON_LNAME = "lname";
    static final String JSON_ROLE = "role";
    public ProjFile(CourseSiteGeneratorApp initApp) {
        app = initApp;
    }

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        ProjData dataManager = (ProjData)data;
        
        JsonArrayBuilder projectArrayBuilder = Json.createArrayBuilder();
        ObservableList<Team> teams = dataManager.getTeams();
        for (Team t: teams){
            JsonObject teamJson = Json.createObjectBuilder()
                    .add(JSON_NAME, t.getName())
                    .add(JSON_COLOR, t.getColor())
                    .add(JSON_TCOLOR, t.getTColor())
                    .add(JSON_LINK, t.getLink())
                    .build();
            projectArrayBuilder.add(teamJson);
        }
        JsonArray teamsArray = projectArrayBuilder.build();
        
        JsonArrayBuilder studentArrayBuilder = Json.createArrayBuilder();
        ObservableList<Student> students = dataManager.getStudents();
        for (Student s: students){
            JsonObject studentJson = Json.createObjectBuilder()
                    .add(JSON_FNAME, s.getFName())
                    .add(JSON_LNAME, s.getLName())
                    .add(JSON_TEAM, s.getTeam())
                    .add(JSON_ROLE, s.getRole())
                    .build();
            studentArrayBuilder.add(studentJson);
        }
        JsonArray studentArray = studentArrayBuilder.build();
        
        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_TEAMS, teamsArray)
                .add(JSON_STUDENTS, studentArray)
                .build();
        filedata = dataManagerJSO;
    }
    
    public JsonObject getFileData(){
        return filedata;
    }
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        ProjData dataManager = (ProjData)data;
        //REWRITE THIS, I THINK I AM GOING TO CHANGE HOW THIS IS BUILT
        JsonObject json = loadJSONFile(filePath);
        JsonArray jsonTeamArray = json.getJsonArray(JSON_TEAMS);
        for (int i = 0; i < jsonTeamArray.size(); i++){
            JsonObject jsonTeam = jsonTeamArray.getJsonObject(i);
            String name = jsonTeam.getString(JSON_NAME);
            String color = jsonTeam.getString(JSON_COLOR);
            String tcolor = jsonTeam.getString(JSON_TCOLOR);
            String link = jsonTeam.getString(JSON_LINK);
            dataManager.addTeam(name, color, tcolor, link);
        }
        JsonArray jsonStudentArray = json.getJsonArray(JSON_STUDENTS);
        for (int i = 0; i < jsonStudentArray.size(); i++){
            JsonObject jsonStudent = jsonStudentArray.getJsonObject(i);
            String fname = jsonStudent.getString(JSON_FNAME);
            String lname = jsonStudent.getString(JSON_LNAME);
            String team = jsonStudent.getString(JSON_TEAM);
            String role = jsonStudent.getString(JSON_ROLE);
            //
            dataManager.addStudent(fname, lname, dataManager.getTeam(team), role);
        }
        ((AppWorkspace)app.getWorkspaceComponent()).getProjWorkspace().reloadWorkspace(dataManager);
        
    }

    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject().getJsonObject("Proj");
        is.close();
        return json;
    }
    
    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        ProjData dataManager = (ProjData) data;

        JsonArrayBuilder projectArrayBuilder = Json.createArrayBuilder();
        ObservableList<Team> teams = dataManager.getTeams();
        for (Team t : teams) {
            JsonObject teamJson = Json.createObjectBuilder()
                    .add(JSON_NAME, t.getName())
                    .add(JSON_COLOR, t.getColor())
                    .add(JSON_TCOLOR, t.getTColor())
                    .add(JSON_LINK, t.getLink())
                    .build();
            projectArrayBuilder.add(teamJson);
        }
        JsonArray teamsArray = projectArrayBuilder.build();

        JsonArrayBuilder studentArrayBuilder = Json.createArrayBuilder();
        ObservableList<Student> students = dataManager.getStudents();
        for (Student s : students) {
            JsonObject studentJson = Json.createObjectBuilder()
                    .add(JSON_FNAME, s.getFName())
                    .add(JSON_LNAME, s.getLName())
                    .add(JSON_TEAM, s.getTeam())
                    .add(JSON_ROLE, s.getRole())
                    .build();
            studentArrayBuilder.add(studentJson);
        }
        JsonArray studentArray = studentArrayBuilder.build();

        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_TEAM, teamsArray)
                .add(JSON_STUDENTS, studentArray)
                .build();
        
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataManagerJSO);
        jsonWriter.close();

        // INIT THE WRITER
        OutputStream os = new FileOutputStream(filePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();
    }

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
