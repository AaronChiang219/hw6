/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pm.workspace;

import csg.data.AppData;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.StringConverter;
import jtps.jTPS;
import pm.data.ProjData;
import pm.data.Student;
import pm.data.Team;
import properties_manager.PropertiesManager;
import tam.CourseSiteGeneratorApp;
import tam.CourseSiteGeneratorProp;

/**
 *
 * @author Aaron
 */
public class ProjWorkspace extends AppWorkspaceComponent{
    CourseSiteGeneratorApp app;
    VBox TeamBox;
    VBox StudentBox;
    BorderPane Project;
    ProjController controller;
    jTPS jTPS;
    
    Label TeamsLabel;
    Label StudentsLabel;
    Label NameLabel;
    Label ColorLabel;
    Label TextColorLabel;
    Label LinkLabel;
    Label EditLabel;
    Label EditLabel2;
    
    ColorPicker colorPicker1;
    ColorPicker colorPicker2;
    
    TextField NameTextField;
    TextField LinkTextField;
    
    Label FNameLabel;
    Label LNameLabel;
    Label TeamLabel;
    Label RoleLabel;
    
    TextField FNameTextField;
    TextField LNameTextField;
    
    ComboBox TeamCombo;
    TextField RoleTextField;
    
    Button addButton;
    Button clearButton;
    
    Button addButton2;
    Button clearButton2;
    
    TableView TeamGrid;
    TableView StudentGrid;
    
    public ProjWorkspace(CourseSiteGeneratorApp initApp){
        app = initApp;
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        jTPS = new jTPS();
        TeamBox = new VBox();
        TeamBox.setAlignment(Pos.CENTER);
        TeamBox.setMaxWidth(1000);
        TeamBox.setStyle("-fx-border-color: black");
        TeamsLabel = new Label(props.getProperty(CourseSiteGeneratorProp.TEAMS_TEXT.toString()));
        TeamGrid = new TableView();
        ProjData data = ((AppData)app.getDataComponent()).getProjData();
        ObservableList<Team> teamData = data.getTeams();
        TeamGrid.setItems(teamData);
        
        TableColumn NameCol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.TEAM_NAME_COL.toString()));
        NameCol.setCellValueFactory(new PropertyValueFactory<Team,String>("Name"));
        
        TableColumn ColorCol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.TEAM_COLOR_COL.toString()));
        ColorCol.setCellValueFactory(new PropertyValueFactory<Team,String>("Color"));
        
        TableColumn TColorCol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.TEAM_TCOLOR_COL.toString()));
        TColorCol.setCellValueFactory(new PropertyValueFactory<Team,String>("TColor"));
        
        TableColumn LinkCol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.TEAM_LINK_COL.toString()));
        LinkCol.setCellValueFactory(new PropertyValueFactory<Team,String>("Link"));
        
        TeamGrid.getColumns().addAll(NameCol, ColorCol, TColorCol, LinkCol);
        
        TeamGrid.setMaxHeight(400);
        TeamGrid.setMaxWidth(1000);
        
        EditLabel = new Label(props.getProperty(CourseSiteGeneratorProp.ADD_UPDATE_BUTTON_TEXT.toString()));
        NameLabel = new Label(props.getProperty(CourseSiteGeneratorProp.NAME_TEXT.toString()));
        ColorLabel = new Label(props.getProperty(CourseSiteGeneratorProp.COLOR_TEXT.toString()));
        TextColorLabel = new Label(props.getProperty(CourseSiteGeneratorProp.TEXT_COLOR_TEXT.toString()));
        LinkLabel = new Label(props.getProperty(CourseSiteGeneratorProp.LINK_TEXT.toString()));
        colorPicker1 = new ColorPicker();
        colorPicker2 = new ColorPicker();
        
        NameTextField = new TextField();
        LinkTextField = new TextField();
        
        addButton = new Button(props.getProperty(CourseSiteGeneratorProp.ADD_UPDATE_BUTTON_TEXT.toString()));
        clearButton = new Button(props.getProperty(CourseSiteGeneratorProp.CLEAR_BUTTON_TEXT.toString()));
        
        VBox blah = new VBox();
        blah.setAlignment(Pos.CENTER);
        blah.setStyle("-fx-background-color: #ccccff; -fx-border-color:black");
        blah.getChildren().add(TeamsLabel);
                
        TeamBox.getChildren().add(blah);
        TeamBox.getChildren().add(TeamGrid);
        
        TilePane bleh = new TilePane();
        bleh.setPrefColumns(2);
        bleh.setAlignment(Pos.CENTER);
        bleh.setMaxWidth(800);
        VBox bazam = new VBox();
        bazam.setAlignment(Pos.CENTER);
        bazam.setStyle("-fx-background-color: #ccccff; -fx-border-color:black");
        bazam.getChildren().add(EditLabel);
        TeamBox.getChildren().add(bazam);
        bleh.getChildren().add(NameLabel);
        bleh.getChildren().add(NameTextField);
        
        bleh.getChildren().add(ColorLabel);
        bleh.getChildren().add(colorPicker1);
        bleh.getChildren().add(TextColorLabel);
        bleh.getChildren().add(colorPicker2);
        
        bleh.getChildren().add(LinkLabel);
        bleh.getChildren().add(LinkTextField);
        bleh.getChildren().add(addButton);
        bleh.getChildren().add(clearButton);
        
        TeamBox.getChildren().add(bleh);
        
        StudentBox = new VBox();
        StudentBox.setStyle("-fx-border-color: black");
        StudentBox.setMaxWidth(1000);
        StudentBox.setAlignment(Pos.CENTER);
        StudentsLabel = new Label(props.getProperty(CourseSiteGeneratorProp.STUDENT_TEXT.toString()));
        StudentGrid = new TableView();
        StudentGrid.setMaxWidth(1000);
        ObservableList<Student> studentData = data.getStudents();
        StudentGrid.setItems(studentData);
        
        
        TableColumn FNameCol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.STUDENT_FNAME_COL.toString()));
        FNameCol.setCellValueFactory(new PropertyValueFactory<Student,String>("FName"));
        
        TableColumn LNameCol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.STUDENT_LNAME_COL.toString()));
        LNameCol.setCellValueFactory(new PropertyValueFactory<Student,String>("LName"));
        
        TableColumn TeamCol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.STUDENT_TEAM_COL.toString()));
        TeamCol.setCellValueFactory(new PropertyValueFactory<Student,String>("Team"));
        
        TableColumn RoleCol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.STUDENT_ROLE_COL.toString()));
        RoleCol.setCellValueFactory(new PropertyValueFactory<Student,String>("Role"));
        
        StudentGrid.getColumns().addAll(FNameCol, LNameCol, TeamCol, RoleCol);
        
        EditLabel2 = new Label(props.getProperty(CourseSiteGeneratorProp.ADD_UPDATE_BUTTON_TEXT.toString()));
        FNameLabel = new Label(props.getProperty(CourseSiteGeneratorProp.FNAME_TEXT.toString()));
        LNameLabel = new Label(props.getProperty(CourseSiteGeneratorProp.LNAME_TEXT.toString()));
        TeamLabel = new Label(props.getProperty(CourseSiteGeneratorProp.TEAM_TEXT.toString()));
        RoleLabel = new Label(props.getProperty(CourseSiteGeneratorProp.ROLE_TEXT.toString()));
        
        FNameTextField = new TextField();
        LNameTextField = new TextField();
        TeamCombo = new ComboBox();
        TeamCombo.setItems(teamData);
        TeamCombo.setCellFactory(
            new Callback<ListView<Team>, ListCell<Team>>() {
                @Override
                public ListCell<Team> call (ListView<Team> t){
                    return new ListCell<Team>(){
                     @Override
                     protected void updateItem(Team item, boolean empty){
                         super.updateItem(item, empty);
                         if (empty){
                             setText("");
                         } else {
                             setText(item.getName());
                         }
                     }
                    };
                }
            }
        );
        TeamCombo.setConverter(new StringConverter<Team>() {
            @Override
            public String toString(Team object) {
               if(object!=null){
                   return object.getName();
               } else {
                   return null;
               }
            }

            @Override
            public Team fromString(String string) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
            
        });
        RoleTextField = new TextField();
        
        addButton2 = new Button(props.getProperty(CourseSiteGeneratorProp.ADD_UPDATE_BUTTON_TEXT.toString()));
        clearButton2 = new Button(props.getProperty(CourseSiteGeneratorProp.CLEAR_BUTTON_TEXT.toString()));
        
        VBox bam = new VBox();
        bam.setAlignment(Pos.CENTER);
        bam.getChildren().add(StudentsLabel);
        bam.setStyle("-fx-background-color: #ccccff; -fx-border-color:black");
        
        StudentBox.getChildren().add(bam);
        StudentBox.getChildren().add(StudentGrid);
        
        VBox kapow = new VBox();
        kapow.setAlignment(Pos.CENTER);
        kapow.getChildren().add(EditLabel2);
        kapow.setStyle("-fx-background-color: #ccccff; -fx-border-color:black");
        StudentBox.getChildren().add(kapow);
        
        TilePane edits = new TilePane();
        edits.setAlignment(Pos.CENTER);
        edits.setPrefColumns(2);
        edits.setAlignment(Pos.CENTER);
        edits.setMaxWidth(800);
        edits.getChildren().add(FNameLabel);
        edits.getChildren().add(FNameTextField);
        edits.getChildren().add(LNameLabel);
        edits.getChildren().add(LNameTextField);
        edits.getChildren().add(TeamLabel);
        edits.getChildren().add(TeamCombo);
        edits.getChildren().add(RoleLabel);
        edits.getChildren().add(RoleTextField);
        edits.getChildren().add(addButton2);
        edits.getChildren().add(clearButton2);
        
        StudentBox.getChildren().add(edits);
        
        Project = new BorderPane();
        VBox test = new VBox();
        test.setAlignment(Pos.CENTER);
        test.getChildren().addAll(TeamBox, StudentBox);
        Project.setCenter(test);
        
        controller = new ProjController(app);
        addButton.setOnAction(e -> {
           controller.handleAddTeam();
        });
        addButton2.setOnAction(e ->{
            controller.handleAddStudent();
        });
        TeamGrid.setOnMouseClicked(e ->{
            controller.handleTeamEdit();
        });
        StudentGrid.setOnMouseClicked(e -> {
            controller.handleStudentEdit();
        });
        clearButton.setOnAction(e -> {
            controller.clearTeam();
        });
        clearButton2.setOnAction(e -> {
            controller.clearStudent();
        });
        TeamGrid.setOnKeyPressed(e ->{
            controller.handleKeyPress(e.getCode());
        });
        StudentGrid.setOnKeyPressed(e ->{
            controller.handleKeyPress(e.getCode());
        });
    }
    public BorderPane getProject(){
        return Project;
    }
    public Label getTeamsLabel(){
        return TeamsLabel;
    }
    public Label getStudentsLabel(){
        return StudentsLabel;
    }
    public Label getNameLabel(){
        return NameLabel;
    }
    public Label getColorLabel(){
        return ColorLabel;
    }
    public Label getTextColorLabel(){
        return TextColorLabel;
    }
    public Label getLinkLabel(){
        return LinkLabel;
    }
    public Label getEditLabel(){
        return EditLabel;
    }
    public Label getEditLabel2(){
        return EditLabel2;
    }
    
    public TextField getNameTextField(){
        return NameTextField;
    }
    public TextField getLinkTextField(){
        return LinkTextField;
    }
    
    public Label getFNameLabel(){
        return FNameLabel;
    }
    public Label getLNameLabel(){
        return LNameLabel;
    }
    public Label getTeamLabel(){
        return TeamLabel;
    }
    public Label getRoleLabel(){
        return RoleLabel;
    }
    public TextField getFNameTextField(){
        return FNameTextField;
    }
    public TextField getLNameTextField(){
        return LNameTextField;
    }
    public ComboBox getTeamCombo(){
        return TeamCombo;
    }
    public TextField getRoleTextField(){
        return RoleTextField;
    }
    public Button getaddButton(){
        return addButton;
    }
    public Button getclearButton(){
        return clearButton;
    }
    public Button getaddButton2(){
        return addButton2;
    }
    public Button getclearButton2(){
        return clearButton2;
    }
    public TableView getTeamGrid(){
        return TeamGrid;
    }
    public TableView getStudentGrid(){
        return StudentGrid;
    }
    public ColorPicker getColorPicker1(){
        return colorPicker1;
    }
    public ColorPicker getColorPicker2(){
        return colorPicker2;
    }
    public jTPS getjTPS(){
        return jTPS;
    }
    public void setjTPS(jTPS j){
        jTPS = j;
    }
    @Override
    public void resetWorkspace() {
        getTeamGrid().getColumns().clear();
        getStudentGrid().getColumns().clear();
        setjTPS(new jTPS());
        
    }

    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {
        ProjData data = (ProjData)dataComponent;
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        getStudentGrid().setItems(data.getStudents());
        getTeamGrid().setItems(data.getTeams());
        TableColumn FNameCol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.STUDENT_FNAME_COL.toString()));
        FNameCol.setCellValueFactory(new PropertyValueFactory<Student, String>("FName"));

        TableColumn LNameCol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.STUDENT_LNAME_COL.toString()));
        LNameCol.setCellValueFactory(new PropertyValueFactory<Student, String>("LName"));

        TableColumn TeamCol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.STUDENT_TEAM_COL.toString()));
        TeamCol.setCellValueFactory(new PropertyValueFactory<Student, String>("Team"));

        TableColumn RoleCol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.STUDENT_ROLE_COL.toString()));
        RoleCol.setCellValueFactory(new PropertyValueFactory<Student, String>("Role"));

        getStudentGrid().getColumns().addAll(FNameCol, LNameCol, TeamCol, RoleCol);
        
        TableColumn NameCol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.TEAM_NAME_COL.toString()));
        NameCol.setCellValueFactory(new PropertyValueFactory<Team, String>("Name"));

        TableColumn ColorCol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.TEAM_COLOR_COL.toString()));
        ColorCol.setCellValueFactory(new PropertyValueFactory<Team, String>("Color"));

        TableColumn TColorCol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.TEAM_TCOLOR_COL.toString()));
        TColorCol.setCellValueFactory(new PropertyValueFactory<Team, String>("TColor"));

        TableColumn LinkCol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.TEAM_LINK_COL.toString()));
        LinkCol.setCellValueFactory(new PropertyValueFactory<Team, String>("Link"));

        getTeamGrid().getColumns().addAll(NameCol, ColorCol, TColorCol, LinkCol);
    }
    
}
