/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pm.workspace;

import csg.data.AppData;
import jtps.jTPS_Transaction;
import pm.data.ProjData;
import pm.data.Student;
import pm.data.Team;
import tam.CourseSiteGeneratorApp;

/**
 *
 * @author caaro
 */
public class Student_Add_Transaction implements jTPS_Transaction{
    CourseSiteGeneratorApp app;
    private String fname;
    private String lname;
    private Team team;
    private String role;
    public Student_Add_Transaction(CourseSiteGeneratorApp a, String f, String l, Team t, String r){
        app = a;
        fname = f;
        lname = l;
        team = t;
        role = r;
    }
    @Override
    public void doTransaction() {
        ProjData data = ((AppData)app.getDataComponent()).getProjData();
        data.addStudent(fname, lname, team, role);
        
    }

    @Override
    public void undoTransaction() {
        ProjData data = ((AppData)app.getDataComponent()).getProjData();
        data.removeStudent(fname, lname);
    }
    
}
