/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pm.workspace;
import csg.data.AppData;
import csg.workspace.AppWorkspace;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.scene.control.Button;
import djf.controller.AppFileController;
import djf.ui.AppGUI;
import static tam.CourseSiteGeneratorProp.*;
import djf.ui.AppMessageDialogSingleton;
import djf.ui.AppYesNoCancelDialogSingleton;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import jtps.jTPS;
import jtps.jTPS_Transaction;
import pm.data.ProjData;
import pm.data.Student;
import pm.data.Team;
import properties_manager.PropertiesManager;
import tam.CourseSiteGeneratorApp;
import tam.CourseSiteGeneratorProp;
/**
 *
 * @author caaro
 */
public class ProjController {
    CourseSiteGeneratorApp app;
    
    public ProjController(CourseSiteGeneratorApp initApp) {
        app = initApp;
    }
    
    private void markWorkAsEdited(){
        AppGUI gui = app.getGUI();
        gui.getFileController().markAsEdited(gui);
        
    }
    
    public void handleAddTeam(){
        ProjWorkspace workspace = ((AppWorkspace)app.getWorkspaceComponent()).getProjWorkspace();
        TextField nameTextField = workspace.getNameTextField();
        TextField linkTextField = workspace.getLinkTextField();
        ColorPicker colorpicker = workspace.getColorPicker1();
        ColorPicker tcolorpicker = workspace.getColorPicker2();
        String name = nameTextField.getText();
        String link = linkTextField.getText();
        String color = colorpicker.getValue().toString();
        String tcolor = tcolorpicker.getValue().toString();
        
        ProjData data = ((AppData)app.getDataComponent()).getProjData();
        if (data.containsTeam(name)){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show("Error message to be filled in by xml later", "same");
            
        } else {
            jTPS_Transaction add = new Team_Add_Transaction(app, name, color, tcolor, link);
            jTPS jTPS = workspace.getjTPS();
            jTPS.addTransaction(add);
            //data.addTeam(name, color, tcolor, link);
            nameTextField.setText("");
            linkTextField.setText("");
            colorpicker.setValue(Color.WHITE);
            tcolorpicker.setValue(Color.WHITE);
            nameTextField.requestFocus();
            ComboBox box = workspace.getTeamCombo();
            //box.getItems().add(name);
            markWorkAsEdited();
        }
    }
    public void handleAddStudent(){
        ProjWorkspace workspace = ((AppWorkspace)app.getWorkspaceComponent()).getProjWorkspace();
        TextField fnameTextField = workspace.getFNameTextField();
        TextField lnameTextField = workspace.getLNameTextField();
        ComboBox teamCombo = workspace.getTeamCombo();
        TextField roleTextField = workspace.getRoleTextField();
        String fname = fnameTextField.getText();
        String lname = lnameTextField.getText();
        Team team = (Team)teamCombo.getValue();
       // String team = teamCombo.getValue().toString();
        String role = roleTextField.getText();
        ProjData data = ((AppData) app.getDataComponent()).getProjData();
        if (data.containsStudent(fname, lname)){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show("Error message to be filled in by xml later", "same");
        } else {
            jTPS_Transaction add = new Student_Add_Transaction(app, fname, lname, team, role);
            jTPS jTPS = workspace.getjTPS();
            jTPS.addTransaction(add);
            //data.addStudent(fname, lname, team, role);
            fnameTextField.setText("");
            lnameTextField.setText("");
            roleTextField.setText("");
            teamCombo.setValue(null);
            fnameTextField.requestFocus();
            markWorkAsEdited();
        }
    }
    public void handleKeyPress(KeyCode code){
        if (code == KeyCode.DELETE){
            ProjWorkspace workspace = ((AppWorkspace)app.getWorkspaceComponent()).getProjWorkspace();
            TableView teamTable = workspace.getTeamGrid();
            TableView studentTable = workspace.getStudentGrid();
            ProjData data = ((AppData)app.getDataComponent()).getProjData();
            Object selectedItem = teamTable.getSelectionModel().getSelectedItem();
            if (selectedItem!=null){
                Team team = (Team)selectedItem;
                String name = team.getName();
                String color = team.getColor();
                String tcolor = team.getTColor();
                String link = team.getLink();
                jTPS_Transaction del = new Team_Delete_Transaction(app, name, color, tcolor, link);
                jTPS jTPS = workspace.getjTPS();
                jTPS.addTransaction(del);
                markWorkAsEdited();
                //workspace.getTeamCombo().getItems().remove(name);
            }
            selectedItem = studentTable.getSelectionModel().getSelectedItem();
            if (selectedItem!=null){
                Student student = (Student)selectedItem;
                String fname = student.getFName();
                String lname = student.getLName();
                Team team = student.getTeamO();
                String role = student.getRole();
                jTPS_Transaction del = new Student_Delete_Transaction(app, fname, lname, team, role);
                jTPS jTPS = workspace.getjTPS();
                jTPS.addTransaction(del);
                markWorkAsEdited();
            }
        }
    }
    
    public void handleTeamEdit(){
        ProjWorkspace workspace = ((AppWorkspace)app.getWorkspaceComponent()).getProjWorkspace();
        TableView teamTable = workspace.getTeamGrid();
        Object selectedItem = teamTable.getSelectionModel().getSelectedItem();
        if (selectedItem == null){
            return;
        }
        Team team = (Team)selectedItem;
        TextField nameTextField = workspace.getNameTextField();
        TextField linkTextField = workspace.getLinkTextField();
        ColorPicker colorpicker = workspace.getColorPicker1();
        ColorPicker tcolorpicker = workspace.getColorPicker2();
        String name = team.getName();
        String link = team.getLink();
        String color = team.getColor();
        String tcolor = team.getTColor();
        nameTextField.setText(name);
        linkTextField.setText(link);
        colorpicker.setValue(Color.web(color));
        tcolorpicker.setValue(Color.web(tcolor));
        TableView studentTable = workspace.getStudentGrid();
        Button button = workspace.getaddButton();
        button.setOnAction(e ->{
            updateTeam(team);
        });
        
    }
    
    public void handleStudentEdit(){
        ProjWorkspace workspace = ((AppWorkspace) app.getWorkspaceComponent()).getProjWorkspace();
        TableView studentTable = workspace.getStudentGrid();
        Object selectedItem = studentTable.getSelectionModel().getSelectedItem();
        if (selectedItem == null){
            return;
        }
        ProjData data = ((AppData)app.getDataComponent()).getProjData();
        Student student = (Student)selectedItem;
        TextField fnameTextField = workspace.getFNameTextField();
        TextField lnameTextField = workspace.getLNameTextField();
        ComboBox teamCombo = workspace.getTeamCombo();
        TextField roleTextField = workspace.getRoleTextField();
        String fname = student.getFName();
        String lname = student.getLName();
        String team = student.getTeam();
        String role = student.getRole();
        fnameTextField.setText(fname);
        lnameTextField.setText(lname);
        teamCombo.setValue(data.getTeam(team));
        roleTextField.setText(role);
        Button button = workspace.getaddButton2();
        button.setOnAction(e ->{
           updateStudent(student); 
        });
        
    }
    
    public void updateTeam(Team team){
        ProjWorkspace workspace = ((AppWorkspace) app.getWorkspaceComponent()).getProjWorkspace();
        TextField nameTextField = workspace.getNameTextField();
        TextField linkTextField = workspace.getLinkTextField();
        ColorPicker colorpicker = workspace.getColorPicker1();
        ColorPicker tcolorpicker = workspace.getColorPicker2();
        String name = nameTextField.getText();
        String link = linkTextField.getText();
        String color = colorpicker.getValue().toString();
        String tcolor = tcolorpicker.getValue().toString();
        ProjData data = ((AppData)app.getDataComponent()).getProjData();
        if (data.containsTeam(name) && !name.equals(team.getName())){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show("Error message to be filled in by xml later", "same");
        } else {
            jTPS_Transaction edit = new Team_Edit_Transaction(app, team, name, color, tcolor, link);
            jTPS jTPS = workspace.getjTPS();
            jTPS.addTransaction(edit);
            /*team.setColor(color);
            team.setLink(link);
            team.setName(name);
            team.setTColor(tcolor);*/
            nameTextField.setText("");
            linkTextField.setText("");
            colorpicker.setValue(Color.WHITE);
            tcolorpicker.setValue(Color.WHITE);
            TableView studentTable = workspace.getStudentGrid();
            TableView teamTable = workspace.getTeamGrid();
            ((TableColumn)studentTable.getColumns().get(0)).setVisible(false);
            ((TableColumn)studentTable.getColumns().get(0)).setVisible(true);
            ((TableColumn)teamTable.getColumns().get(0)).setVisible(false);
            ((TableColumn)teamTable.getColumns().get(0)).setVisible(true);
            Button button = workspace.getaddButton();
            button.setOnAction(e->{
                handleAddTeam();
            });
            markWorkAsEdited();
        }
    }
    
    public void updateStudent(Student student){
        ProjWorkspace workspace = ((AppWorkspace)app.getWorkspaceComponent()).getProjWorkspace();
        TextField fnameTextField = workspace.getFNameTextField();
        TextField lnameTextField = workspace.getLNameTextField();
        ComboBox teamCombo = workspace.getTeamCombo();
        TextField roleTextField = workspace.getRoleTextField();
        String fname = fnameTextField.getText();
        String lname = lnameTextField.getText();
        Team team = (Team) teamCombo.getValue();
        String role = roleTextField.getText();
        ProjData data = ((AppData) app.getDataComponent()).getProjData();
        if (data.containsStudent(fname, lname) && !fname.equals(student.getFName())){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show("Error message to be filled in by xml later", "same");
        } else {
            
            jTPS_Transaction edit = new Student_Edit_Transaction(app, fname, lname, team,role, student);
            jTPS jTPS = workspace.getjTPS();
            jTPS.addTransaction(edit);
            fnameTextField.setText("");
            lnameTextField.setText("");
            roleTextField.setText("");
            teamCombo.setValue("");
            Button button = workspace.getaddButton2();
            button.setOnAction(e ->{
                handleAddStudent();
            });
            markWorkAsEdited();
        }
    }
    
    public void clearTeam(){
        ProjWorkspace workspace = ((AppWorkspace)app.getWorkspaceComponent()).getProjWorkspace();
        TextField nameTextField = workspace.getNameTextField();
        TextField linkTextField = workspace.getLinkTextField();
        ColorPicker colorpicker = workspace.getColorPicker1();
        ColorPicker tcolorpicker = workspace.getColorPicker2();
        nameTextField.setText("");
        linkTextField.setText("");
        colorpicker.setValue(Color.WHITE);
        tcolorpicker.setValue(Color.WHITE);
        nameTextField.requestFocus();
        Button button = workspace.getaddButton();
        button.setOnAction(e -> {
            handleAddTeam();
        });
    }
    public void clearStudent(){
        ProjWorkspace workspace = ((AppWorkspace)app.getWorkspaceComponent()).getProjWorkspace();
        TextField fnameTextField = workspace.getFNameTextField();
        TextField lnameTextField = workspace.getLNameTextField();
        ComboBox teamCombo = workspace.getTeamCombo();
        TextField roleTextField = workspace.getRoleTextField();
        fnameTextField.setText("");
        lnameTextField.setText("");
        teamCombo.setValue("");
        roleTextField.setText("");
        fnameTextField.requestFocus();
        Button button = workspace.getaddButton2();
        button.setOnAction(e ->{
            handleAddStudent();
        });
    }
}
