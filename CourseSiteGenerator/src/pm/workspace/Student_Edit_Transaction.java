/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pm.workspace;

import jtps.jTPS_Transaction;
import pm.data.Student;
import pm.data.Team;
import tam.CourseSiteGeneratorApp;

/**
 *
 * @author caaro
 */
public class Student_Edit_Transaction implements jTPS_Transaction{
    CourseSiteGeneratorApp app;
    private String fname;
    private String lname;
    private Team team;
    private String role;
    private Student student;
    private String ofname;
    private String olname; 
    private Team oteam;
    private String orole;
    
    public Student_Edit_Transaction(CourseSiteGeneratorApp a, String f, String l, Team t, String r, Student s){
        app = a;
        fname = f;
        lname = l;
        team = t;
        role = r;
        student = s;
        ofname = s.getFName();
        olname = s.getLName();
        oteam = s.getTeamO();
        orole = s.getRole();
    }
    @Override
    public void doTransaction() {
        student.setFName(fname);
        student.setLName(lname);
        student.setTeam(team);
        student.setRole(role);
    }

    @Override
    public void undoTransaction() {
        student.setFName(ofname);
        student.setLName(olname);
        student.setTeam(oteam);
        student.setRole(orole);
    }
    
}
