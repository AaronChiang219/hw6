/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pm.workspace;

import jtps.jTPS_Transaction;
import pm.data.Team;
import tam.CourseSiteGeneratorApp;

/**
 *
 * @author caaro
 */
public class Team_Edit_Transaction implements jTPS_Transaction {
    CourseSiteGeneratorApp app;
    private String name;
    private String color;
    private String tcolor;
    private String link;
    private Team team;
    private String oname;
    private String ocolor;
    private String otcolor;
    private String olink;
    public Team_Edit_Transaction(CourseSiteGeneratorApp a, Team initTeam, String n, String c, String tc, String l){
        app = a;
        name = n;
        color = c;
        tcolor = tc;
        link = l;
        team = initTeam;
        oname = team.getName();
        ocolor = team.getColor();
        otcolor = team.getTColor();
        olink = team.getLink();
    }
    @Override
    public void doTransaction() {
       team.setColor(color);
       team.setLink(link);
       team.setName(name);
       team.setTColor(tcolor);
    }

    @Override
    public void undoTransaction() {
        team.setColor(ocolor);
        team.setLink(olink);
        team.setName(oname);
        team.setTColor(otcolor);
    }
    
}
