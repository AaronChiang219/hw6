/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pm.workspace;

import csg.data.AppData;
import jtps.jTPS_Transaction;
import pm.data.ProjData;
import tam.CourseSiteGeneratorApp;

/**
 *
 * @author caaro
 */
public class Team_Add_Transaction implements jTPS_Transaction {
    CourseSiteGeneratorApp app;
    private String name;
    private String color;
    private String tcolor;
    private String link;
    public Team_Add_Transaction(CourseSiteGeneratorApp a, String n, String c, String tc, String l){
        app = a;
        name = n;
        color = c;
        tcolor = tc;
        link = l;
    }
    @Override
    public void doTransaction() {
        ProjData data = ((AppData)app.getDataComponent()).getProjData();
        data.addTeam(name, color, tcolor, link);
    }

    @Override
    public void undoTransaction() {
        ProjData data = ((AppData)app.getDataComponent()).getProjData();
        data.removeTeam(name);
    }
    
}
