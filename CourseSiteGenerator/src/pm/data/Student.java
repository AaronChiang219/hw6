/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pm.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
/**
 *
 * @author Aaron
 */
public class Student<E extends Comparable<E>> implements Comparable<E> {
    
    public final StringProperty fname;
    public final StringProperty lname;
    public final Team team;
    public final StringProperty role;
    
    public Student(String initf, String initl, Team initTeam, String initRole){
        fname = new SimpleStringProperty(initf);
        lname = new SimpleStringProperty(initl);
        team = initTeam;
        role = new SimpleStringProperty(initRole);
    }
    
    public String getFName(){
        return fname.get();
    }
    public void setFName(String s){
        fname.set(s);
    }
    public String getLName(){
        return lname.get();
    }
    public void setLName(String s){
        lname.set(s);
    }
    public String getTeam(){
        return team.getName();
    }
    public Team getTeamO(){
        return team;
    }
    public void setTeam(Team s){
        team.setColor(s.getColor());
        team.setLink(s.getLink());
        team.setName(s.getName());
        team.setTColor(s.getTColor());
        
    }
    public String getRole(){
        return role.get();
    }
    public void setRole(String s){
        role.set(s);
    }

    @Override
    public int compareTo(E o) {
        return getFName().compareTo(((Student)o).getFName());
    }
    
}
    
