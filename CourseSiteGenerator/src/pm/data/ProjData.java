/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pm.data;

import djf.components.AppDataComponent;
import java.util.Collections;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import tam.CourseSiteGeneratorApp;

/**
 *
 * @author Aaron
 */
public class ProjData implements AppDataComponent{
    CourseSiteGeneratorApp app;
    ObservableList<Student> Students;
    ObservableList<Team> Teams;
    public ProjData(CourseSiteGeneratorApp initApp){
        app = initApp;
        Students = FXCollections.observableArrayList();
        Teams = FXCollections.observableArrayList();
        
    }
    
    public ObservableList getTeams(){
        return Teams;
    }
    public ObservableList getStudents(){
        return Students;
    }
    public void addStudent(String fname, String lname, Team team, String role){
        Student stu = new Student(fname, lname, team,role);
        if (!containsStudent(fname, lname)){
            Students.add(stu);
        }
        Collections.sort(Students);
    }
    public void addTeam(String name, String color, String tcolor, String link){
        Team team = new Team(name, color, tcolor, link);
        if(!containsTeam(name)){
            Teams.add(team);
        }
        Collections.sort(Teams);
    }
    public boolean containsStudent(String fname, String lname){
        for(Student s: Students){
            if (s.getFName().equals(fname) && s.getLName().equals(lname)){
                return true;
            }
        }
        return false;
    }
    public boolean containsTeam(String name){
        for (Team t: Teams){
            if (t.getName().equals(name)){
                return true;
            }
        }
        return false;
    }
    public Team getTeam(String name){
        for (Team t: Teams){
            if (t.getName().equals(name)){
                return t;
            }
        }
        return null;
    }
    public void removeStudent(String fname, String lname){
        for (Student s: Students){
            if (s.getFName().equals(fname) && s.getLName().equals(lname)){
                Students.remove(s);
                return;
            }
        }
    }
    public void removeTeam(String name){
        for (Team t: Teams){
            if (t.getName().equals(name)){
                Teams.remove(t);
                return;
            }
        }
    }

    @Override
    public void resetData() {
        Students.clear();
        Teams.clear();
    }
    
}
