/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pm.data;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
/**
 *
 * @author Aaron
 */
public class Team<E extends Comparable<E>> implements Comparable<E> {
    public final StringProperty name;
    public final StringProperty color;
    public final StringProperty tcolor;
    public final StringProperty link;
    public Team(String initName, String initColor, String initTColor, String initLink){
        name = new SimpleStringProperty(initName);
        color = new SimpleStringProperty(initColor);
        tcolor = new SimpleStringProperty(initTColor);
        link = new SimpleStringProperty(initLink);
    }
    public String getName(){
        return name.get();
    }
    public void setName(String s){
        name.set(s);
    }
    public String getColor(){
        return color.get();
    }
    public void setColor(String s){
        color.set(s);
    }
    public String getTColor(){
        return tcolor.get();
    }
    public void setTColor(String s){
        tcolor.set(s);
    }
    public String getLink(){
        return link.get();
    }
    public void setLink(String s){
        link.set(s);
    }

    @Override
    public int compareTo(E o) {
        return getName().compareTo(((Team)o).getName());
    }
    
}
