/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.style;

/**
 *
 * @author Aaron
 */
import cm.workspace.CourseWorkspace;
import csg.workspace.AppWorkspace;
import djf.AppTemplate;
import djf.components.AppStyleComponent;
import java.util.HashMap;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import pm.workspace.ProjWorkspace;
import rm.workspace.RecWorkspace;
import sm.workspace.SchWorkspace;
import tam.data.TeachingAssistant;
import tam.style.TAStyle;
import tam.workspace.TAWorkspace;
/**
 *
 * @author Aaron
 */
public class AppStyle extends AppStyleComponent {

    private AppTemplate app;
    public static String CLASS_PLAIN_PANE = "plain_pane";

    // THESE ARE THE HEADERS FOR EACH SIDE
    public static String CLASS_HEADER_PANE = "header_pane";
    public static String CLASS_HEADER_LABEL = "header_label";

    // ON THE LEFT WE HAVE THE TA ENTRY
    public static String CLASS_TA_TABLE = "ta_table";
    public static String CLASS_TA_TABLE_COLUMN_HEADER = "ta_table_column_header";
    public static String CLASS_ADD_TA_PANE = "add_ta_pane";
    public static String CLASS_ADD_TA_TEXT_FIELD = "add_ta_text_field";
    public static String CLASS_ADD_TA_BUTTON = "add_ta_button";

    // ON THE RIGHT WE HAVE OUR COMBO BOXES
    public static String COMBO_BOX = "combo_box";
    public static String COMBO_LABEL = "combo_label";

    // ON THE RIGHT WE HAVE THE OFFICE HOURS GRID
    public static String CLASS_OFFICE_HOURS_GRID = "office_hours_grid";
    public static String CLASS_OFFICE_HOURS_GRID_TIME_COLUMN_HEADER_PANE = "office_hours_grid_time_column_header_pane";
    public static String CLASS_OFFICE_HOURS_GRID_TIME_COLUMN_HEADER_LABEL = "office_hours_grid_time_column_header_label";
    public static String CLASS_OFFICE_HOURS_GRID_DAY_COLUMN_HEADER_PANE = "office_hours_grid_day_column_header_pane";
    public static String CLASS_OFFICE_HOURS_GRID_DAY_COLUMN_HEADER_LABEL = "office_hours_grid_day_column_header_label";
    public static String CLASS_OFFICE_HOURS_GRID_TIME_CELL_PANE = "office_hours_grid_time_cell_pane";
    public static String CLASS_OFFICE_HOURS_GRID_TIME_CELL_LABEL = "office_hours_grid_time_cell_label";
    public static String CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE = "office_hours_grid_ta_cell_pane";
    public static String CLASS_OFFICE_HOURS_GRID_TA_CELL_LABEL = "office_hours_grid_ta_cell_label";

    // FOR HIGHLIGHTING CELLS, COLUMNS, AND ROWS
    public static String CLASS_HIGHLIGHTED_GRID_CELL = "highlighted_grid_cell";
    public static String CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN = "highlighted_grid_row_or_column";
    
    public static String CLASS_ADD_UPDATE_BUTTON = "add_update_button";
    public static String CLASS_CLEAR_BUTTON = "clear_button";
    public static String CLASS_EDIT_TEXTFIELD = "edit_textfield";
    public static String CLASS_EDIT_COMBOBOX = "edit_combobox";
    public static String CLASS_EDIT_LABEL = "edit_label";
    public static String CLASS_PAGE_GRID = "page_grid";
    public static String CLASS_SCHEDULE_GRID = "schedule_grid";
    public static String CLASS_RECITATION_GRID = "recitation_grid";
    public static String CLASS_PROJECT_GRID = "project_grid";
    public static String CLASS_STUDENT_GRID = "student_grid";
    public static String CLASS_CHANGE_BUTTON = "change_button";
    public static String CLASS_HEADER_TEXT = "header_text";
    public static String CLASS_TAB = "tab";
    
    public AppStyle(AppTemplate initApp) {
        // KEEP THIS FOR LATER
        app = initApp;

        // LET'S USE THE DEFAULT STYLESHEET SETUP
        super.initStylesheet(app);

        // INIT THE STYLE FOR THE FILE TOOLBAR
        app.getGUI().initFileToolbarStyle();
        initCourseWorkspaceStyle();
        initTAWorkspaceStyle();
        initRecWorkspaceStyle();
        initSchWorkspaceStyle();
        initProjWorkspaceStyle();
        //initCourseWorkspaceStyle();

    }
 private void initTAWorkspaceStyle() {
        // LEFT SIDE - THE HEADER
        TAWorkspace workspaceComponent = ((AppWorkspace)app.getWorkspaceComponent()).getTAWorkspace();
        workspaceComponent.getTAsHeaderBox().getStyleClass().add(CLASS_HEADER_PANE);
        workspaceComponent.getTAsHeaderLabel().getStyleClass().add(CLASS_HEADER_LABEL);

        // LEFT SIDE - THE TABLE
        TableView<TeachingAssistant> taTable = workspaceComponent.getTATable();
        taTable.getStyleClass().add(CLASS_TA_TABLE);
        for (TableColumn tableColumn : taTable.getColumns()) {
            tableColumn.getStyleClass().add(CLASS_TA_TABLE_COLUMN_HEADER);
        }

        // LEFT SIDE - THE TA DATA ENTRY
        workspaceComponent.getAddBox().getStyleClass().add(CLASS_ADD_TA_PANE);
        workspaceComponent.getNameTextField().getStyleClass().add(CLASS_ADD_TA_TEXT_FIELD);
        workspaceComponent.getEmailTextField().getStyleClass().add(CLASS_ADD_TA_TEXT_FIELD);
        workspaceComponent.getAddButton().getStyleClass().add(CLASS_ADD_TA_BUTTON);
        workspaceComponent.getClearButton().getStyleClass().add(CLASS_ADD_TA_BUTTON);
        
        // RIGHT SIDE - SELECT TIME COMBO BOXES
        workspaceComponent.getStartTime().getStyleClass().add(COMBO_BOX);
        workspaceComponent.getEndTime().getStyleClass().add(COMBO_BOX);
        workspaceComponent.getStartLabel().getStyleClass().add(COMBO_LABEL);
        workspaceComponent.getEndLabel().getStyleClass().add(COMBO_LABEL);

        // RIGHT SIDE - THE HEADER
        workspaceComponent.getOfficeHoursSubheaderBox().getStyleClass().add(CLASS_HEADER_PANE);
        workspaceComponent.getOfficeHoursSubheaderLabel().getStyleClass().add(CLASS_HEADER_LABEL);
    }
    
    /**
     * This method initializes the style for all UI components in
     * the office hours grid. Note that this should be called every
     * time a new TA Office Hours Grid is created or loaded.
     */
    public void initOfficeHoursGridStyle() {
        // RIGHT SIDE - THE OFFICE HOURS GRID TIME HEADERS
        TAWorkspace workspaceComponent =  ((AppWorkspace)app.getWorkspaceComponent()).getTAWorkspace();
        workspaceComponent.getOfficeHoursGridPane().getStyleClass().add(CLASS_OFFICE_HOURS_GRID);
        setStyleClassOnAll(workspaceComponent.getOfficeHoursGridTimeHeaderPanes(), CLASS_OFFICE_HOURS_GRID_TIME_COLUMN_HEADER_PANE);
        setStyleClassOnAll(workspaceComponent.getOfficeHoursGridTimeHeaderLabels(), CLASS_OFFICE_HOURS_GRID_TIME_COLUMN_HEADER_LABEL);
        setStyleClassOnAll(workspaceComponent.getOfficeHoursGridDayHeaderPanes(), CLASS_OFFICE_HOURS_GRID_DAY_COLUMN_HEADER_PANE);
        setStyleClassOnAll(workspaceComponent.getOfficeHoursGridDayHeaderLabels(), CLASS_OFFICE_HOURS_GRID_DAY_COLUMN_HEADER_LABEL);
        setStyleClassOnAll(workspaceComponent.getOfficeHoursGridTimeCellPanes(), CLASS_OFFICE_HOURS_GRID_TIME_CELL_PANE);
        setStyleClassOnAll(workspaceComponent.getOfficeHoursGridTimeCellLabels(), CLASS_OFFICE_HOURS_GRID_TIME_CELL_LABEL);
        setStyleClassOnAll(workspaceComponent.getOfficeHoursGridTACellPanes(), CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);
        setStyleClassOnAll(workspaceComponent.getOfficeHoursGridTACellLabels(), CLASS_OFFICE_HOURS_GRID_TA_CELL_LABEL);
    }
    
    private void initCourseWorkspaceStyle(){
        CourseWorkspace workspaceComponent =  ((AppWorkspace)app.getWorkspaceComponent()).getCourseWorkspace();
        workspaceComponent.getbannerLabel().getStyleClass().add(CLASS_EDIT_LABEL);
        workspaceComponent.getlfooterLabel().getStyleClass().add(CLASS_EDIT_LABEL);
        workspaceComponent.getrfooterLabel().getStyleClass().add(CLASS_EDIT_LABEL);
        workspaceComponent.getsubLabel().getStyleClass().add(CLASS_EDIT_LABEL);
        workspaceComponent.getsemLabel().getStyleClass().add(CLASS_EDIT_LABEL);
        workspaceComponent.getyearLabel().getStyleClass().add(CLASS_EDIT_LABEL);
        workspaceComponent.getnumLabel().getStyleClass().add(CLASS_EDIT_LABEL);
        workspaceComponent.gettitleLabel().getStyleClass().add(CLASS_EDIT_LABEL);
        workspaceComponent.getinstLabel().getStyleClass().add(CLASS_EDIT_LABEL);
        workspaceComponent.gethomeLabel().getStyleClass().add(CLASS_EDIT_LABEL);
        workspaceComponent.getexportLabel().getStyleClass().add(CLASS_EDIT_LABEL);
        workspaceComponent.getstyleLabel().getStyleClass().add(CLASS_EDIT_LABEL);
        
        workspaceComponent.getcourseLabel().getStyleClass().add(CLASS_HEADER_TEXT);
        workspaceComponent.gettempLabel().getStyleClass().add(CLASS_HEADER_TEXT);
        
        //workspaceComponent.gettempLabel().getStyleClass().add(CLASS_EDIT_LABEL);
        workspaceComponent.gettempText().getStyleClass().add(CLASS_EDIT_LABEL);
        workspaceComponent.getnoteLabel().getStyleClass().add(CLASS_EDIT_LABEL);
        
        workspaceComponent.getsubCombo().getStyleClass().add(COMBO_BOX);
        workspaceComponent.getsemCombo().getStyleClass().add(COMBO_BOX);
        workspaceComponent.getnumCombo().getStyleClass().add(COMBO_BOX);
        workspaceComponent.getyearCombo().getStyleClass().add(COMBO_BOX);
        workspaceComponent.getstyleCombo().getStyleClass().add(COMBO_BOX);
        
        workspaceComponent.getTitleTextField().getStyleClass().add(CLASS_EDIT_TEXTFIELD);
        workspaceComponent.getInstructorTextField().getStyleClass().add(CLASS_EDIT_TEXTFIELD);
        workspaceComponent.getHomeTextField().getStyleClass().add(CLASS_EDIT_TEXTFIELD);
        
        workspaceComponent.getPageGrid().getStyleClass().add(CLASS_PAGE_GRID);
    }
    private void initRecWorkspaceStyle(){
        RecWorkspace workspaceComponent = ((AppWorkspace) app.getWorkspaceComponent()).getRecWorkspace();
        
        
        workspaceComponent.getSectionTextField().getStyleClass().add(CLASS_EDIT_TEXTFIELD);
        workspaceComponent.getInstructorTextField().getStyleClass().add(CLASS_EDIT_TEXTFIELD);
        workspaceComponent.getTimeTextField().getStyleClass().add(CLASS_EDIT_TEXTFIELD);
        workspaceComponent.getLocationTextField().getStyleClass().add(CLASS_EDIT_TEXTFIELD);
        
        workspaceComponent.getAddEditLabel().getStyleClass().add(CLASS_HEADER_TEXT);
        workspaceComponent.getRecLabel().getStyleClass().add(CLASS_HEADER_TEXT);
        workspaceComponent.getSectionLabel().getStyleClass().add(CLASS_EDIT_LABEL);
        workspaceComponent.getInstructorLabel().getStyleClass().add(CLASS_EDIT_LABEL);
        workspaceComponent.getTimeLabel().getStyleClass().add(CLASS_EDIT_LABEL);
        workspaceComponent.getLocationLabel().getStyleClass().add(CLASS_EDIT_LABEL);
        workspaceComponent.getTALabel().getStyleClass().add(CLASS_EDIT_LABEL);
        workspaceComponent.getTALabel2().getStyleClass().add(CLASS_EDIT_LABEL);
        
        workspaceComponent.getTACombo().getStyleClass().add(COMBO_BOX);
        workspaceComponent.getTACombo2().getStyleClass().add(COMBO_BOX);
        
        workspaceComponent.getaddButton().getStyleClass().add(CLASS_ADD_UPDATE_BUTTON);
        workspaceComponent.getclearButton().getStyleClass().add(CLASS_CLEAR_BUTTON);
        
        workspaceComponent.getRecitationGrid().getStyleClass().add(CLASS_RECITATION_GRID);
    }
    private void initSchWorkspaceStyle(){
        SchWorkspace workspaceComponent = ((AppWorkspace) app.getWorkspaceComponent()).getSchWorkspace();
        
        workspaceComponent.getStartDateLabel().getStyleClass().add(CLASS_EDIT_LABEL);
        workspaceComponent.getEndDateLabel().getStyleClass().add(CLASS_EDIT_LABEL);
        
        workspaceComponent.getStartDatePicker().getStyleClass().add(COMBO_BOX);
        workspaceComponent.getEndDatePicker().getStyleClass().add(COMBO_BOX);
        
        workspaceComponent.getScheduleGrid().getStyleClass().add(CLASS_SCHEDULE_GRID);
        
        workspaceComponent.getCalLabel().getStyleClass().add(CLASS_HEADER_TEXT);
        workspaceComponent.getSchLabel().getStyleClass().add(CLASS_HEADER_TEXT);
        workspaceComponent.getEditLabel().getStyleClass().add(CLASS_HEADER_TEXT);
        workspaceComponent.getTypeLabel().getStyleClass().add(CLASS_EDIT_LABEL);
        workspaceComponent.getDateLabel().getStyleClass().add(CLASS_EDIT_LABEL);
        workspaceComponent.getTimeLabel().getStyleClass().add(CLASS_EDIT_LABEL);
        workspaceComponent.getTitleLabel().getStyleClass().add(CLASS_EDIT_LABEL);
        workspaceComponent.getTopicLabel().getStyleClass().add(CLASS_EDIT_LABEL);
        workspaceComponent.getLinkLabel().getStyleClass().add(CLASS_EDIT_LABEL);
        workspaceComponent.getCriteriaLabel().getStyleClass().add(CLASS_EDIT_LABEL);
        
        workspaceComponent.getTypeCombo().getStyleClass().add(COMBO_BOX);
        workspaceComponent.getDatePicker().getStyleClass().add(COMBO_BOX);
        
        workspaceComponent.getTimeTextField().getStyleClass().add(CLASS_EDIT_TEXTFIELD);
        workspaceComponent.getTitleTextField().getStyleClass().add(CLASS_EDIT_TEXTFIELD);
        workspaceComponent.getTopicTextField().getStyleClass().add(CLASS_EDIT_TEXTFIELD);
        workspaceComponent.getLinkTextField().getStyleClass().add(CLASS_EDIT_TEXTFIELD);
        workspaceComponent.getCriteriaTextField().getStyleClass().add(CLASS_EDIT_TEXTFIELD);
        
        workspaceComponent.getaddButton().getStyleClass().add(CLASS_ADD_UPDATE_BUTTON);
        workspaceComponent.getclearButton().getStyleClass().add(CLASS_CLEAR_BUTTON);
        
        
        
    }
    private void initProjWorkspaceStyle(){
        ProjWorkspace workspaceComponent = ((AppWorkspace) app.getWorkspaceComponent()).getProjWorkspace();
        
        workspaceComponent.getTeamsLabel().getStyleClass().add(CLASS_HEADER_TEXT);
        workspaceComponent.getStudentsLabel().getStyleClass().add(CLASS_HEADER_TEXT);
        
        workspaceComponent.getNameLabel().getStyleClass().add(CLASS_EDIT_LABEL);
        workspaceComponent.getColorLabel().getStyleClass().add(CLASS_EDIT_LABEL);
        workspaceComponent.getTextColorLabel().getStyleClass().add(CLASS_EDIT_LABEL);
        workspaceComponent.getLinkLabel().getStyleClass().add(CLASS_EDIT_LABEL);
        workspaceComponent.getEditLabel().getStyleClass().add(CLASS_HEADER_TEXT);
        workspaceComponent.getEditLabel2().getStyleClass().add(CLASS_HEADER_TEXT);
        
        workspaceComponent.getFNameLabel().getStyleClass().add(CLASS_EDIT_LABEL);
        workspaceComponent.getLNameLabel().getStyleClass().add(CLASS_EDIT_LABEL);
        workspaceComponent.getTeamLabel().getStyleClass().add(CLASS_EDIT_LABEL);
        workspaceComponent.getRoleLabel().getStyleClass().add(CLASS_EDIT_LABEL);
        
        workspaceComponent.getNameTextField().getStyleClass().add(CLASS_EDIT_TEXTFIELD);
        workspaceComponent.getLinkTextField().getStyleClass().add(CLASS_EDIT_TEXTFIELD);
        workspaceComponent.getFNameTextField().getStyleClass().add(CLASS_EDIT_TEXTFIELD);
        workspaceComponent.getLNameTextField().getStyleClass().add(CLASS_EDIT_TEXTFIELD);
        
        workspaceComponent.getTeamCombo().getStyleClass().add(COMBO_BOX);
        workspaceComponent.getRoleTextField().getStyleClass().add(CLASS_EDIT_TEXTFIELD);
        
        workspaceComponent.getaddButton().getStyleClass().add(CLASS_ADD_UPDATE_BUTTON);
        workspaceComponent.getclearButton().getStyleClass().add(CLASS_CLEAR_BUTTON);
        workspaceComponent.getaddButton2().getStyleClass().add(CLASS_ADD_UPDATE_BUTTON);
        workspaceComponent.getclearButton2().getStyleClass().add(CLASS_CLEAR_BUTTON);
        
        workspaceComponent.getTeamGrid().getStyleClass().add(CLASS_PROJECT_GRID);
        workspaceComponent.getStudentGrid().getStyleClass().add(CLASS_STUDENT_GRID);
    }
    /**
     * This helper method initializes the style of all the nodes in the nodes
     * map to a common style, styleClass.
     */
    private void setStyleClassOnAll(HashMap nodes, String styleClass) {
        for (Object nodeObject : nodes.values()) {
            Node n = (Node) nodeObject;
            n.getStyleClass().add(styleClass);
        }
    }
}
