/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import tam.CourseSiteGeneratorApp;
import cm.workspace.CourseWorkspace;
import csg.data.AppData;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import pm.workspace.ProjWorkspace;
import properties_manager.PropertiesManager;
import rm.workspace.RecWorkspace;
import sm.workspace.SchWorkspace;
import tam.CourseSiteGeneratorProp;
import tam.data.TAData;
import tam.workspace.TAWorkspace;

/**
 *
 * @author Aaron
 */
public class AppWorkspace extends AppWorkspaceComponent {
    
    CourseSiteGeneratorApp app;
    TabPane tabPane;
    CourseWorkspace Coursework;
    TAWorkspace TAwork;
    RecWorkspace Recwork;
    SchWorkspace Schwork;
    ProjWorkspace Projwork;
    Tab Coursetab;
    Tab TAtab;
    Tab Rectab;
    Tab Schtab;
    Tab Projtab;
    
    public AppWorkspace(CourseSiteGeneratorApp initApp){
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        Coursework = new CourseWorkspace(initApp);
        tabPane = new TabPane();
        Coursetab = new Tab();
        Coursetab.setText(props.getProperty(CourseSiteGeneratorProp.COURSE_TAB.toString()));
        Coursetab.setContent(Coursework.getCourse());
        tabPane.getTabs().add(Coursetab);
        TAwork = new TAWorkspace(initApp);
        TAtab = new Tab();
        TAtab.setText(props.getProperty(CourseSiteGeneratorProp.TA_TAB.toString()));
        TAtab.setContent(TAwork.getTA());
        tabPane.getTabs().add(TAtab);
        Recwork = new RecWorkspace(initApp);
        Rectab = new Tab();
        Rectab.setText(props.getProperty(CourseSiteGeneratorProp.RECITATION_TAB.toString()));
        Rectab.setContent(Recwork.getRecitation());
        tabPane.getTabs().add(Rectab);
        Schwork = new SchWorkspace(initApp);
        Schtab = new Tab();
        Schtab.setText(props.getProperty(CourseSiteGeneratorProp.SCHEDULE_TAB.toString()));
        Schtab.setContent(Schwork.getSchedule());
        tabPane.getTabs().add(Schtab);
        Projwork = new ProjWorkspace(initApp);
        Projtab = new Tab();
        Projtab.setText(props.getProperty(CourseSiteGeneratorProp.PROJECT_TAB.toString()));
        Projtab.setContent(Projwork.getProject());
        tabPane.getTabs().add(Projtab);
        tabPane.setTabMinWidth(280);
        workspace = new BorderPane();
        ((BorderPane) workspace).setCenter(tabPane);
        workspace.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.Z && e.isControlDown()){
                handleUndo();
            }
            if (e.getCode() == KeyCode.Y && e.isControlDown()){
                handleRedo();
            }
        });
    }
    public CourseWorkspace getCourseWorkspace(){
        return Coursework;
    }
    public TAWorkspace getTAWorkspace(){
        return TAwork;
    }
    public RecWorkspace getRecWorkspace(){
        return Recwork;
    }
    public SchWorkspace getSchWorkspace(){
        return Schwork;
    }
    public ProjWorkspace getProjWorkspace(){
        return Projwork;
    }
    public void handleUndo(){
        Tab selectedTab = tabPane.getSelectionModel().getSelectedItem();
        if (selectedTab==Coursetab){
            Coursework.getjTPS().undoTransaction();
        } else if (selectedTab == TAtab){
            TAwork.getjTPS().undoTransaction();
        } else if (selectedTab == Rectab){
            Recwork.getjTPS().undoTransaction();
        } else if (selectedTab == Schtab){
            Schwork.getjTPS().undoTransaction();
        } else if (selectedTab == Projtab){
            Projwork.getjTPS().undoTransaction();
        }     
    }
    public void handleRedo(){
        Tab selectedTab = tabPane.getSelectionModel().getSelectedItem();
        if (selectedTab == Coursetab) {
            Coursework.getjTPS().doTransaction();
        } else if (selectedTab == TAtab) {
            TAwork.getjTPS().doTransaction();
        } else if (selectedTab == Rectab) {
            Recwork.getjTPS().doTransaction();
        } else if (selectedTab == Schtab) {
            Schwork.getjTPS().doTransaction();
        } else if (selectedTab == Projtab) {
            Projwork.getjTPS().doTransaction();
        }
    }
    @Override
    public void resetWorkspace() {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
       TAwork.resetWorkspace();
       Coursework.resetWorkspace();
       Recwork.resetWorkspace();
       Schwork.resetWorkspace();
       Projwork.resetWorkspace();
    }

    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
      TAwork.reloadWorkspace(((AppData)dataComponent).getTAData());
      Coursework.reloadWorkspace(((AppData)dataComponent).getCourseData());
      Recwork.reloadWorkspace(((AppData)dataComponent).getRecData());
      Schwork.reloadWorkspace(((AppData)dataComponent).getSchData());
      Projwork.reloadWorkspace(((AppData)dataComponent).getProjData());
      
    }
    
}
