/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import cm.data.CourseData;
import djf.components.AppDataComponent;
import csg.workspace.AppWorkspace;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import djf.components.AppDataComponent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import javafx.beans.property.StringProperty;
import pm.data.ProjData;
import properties_manager.PropertiesManager;
import rm.data.RecData;
import sm.data.SchData;
import tam.CourseSiteGeneratorApp;
import tam.CourseSiteGeneratorProp;
import tam.data.TAData;
import tam.workspace.TAWorkspace;
/**
 *
 * @author Aaron
 */
public class AppData implements AppDataComponent{

    CourseSiteGeneratorApp app;
    CourseData coursedata;
    ProjData projdata;
    RecData recdata;
    SchData schdata;
    TAData tadata;
    
    public AppData(CourseSiteGeneratorApp initApp){
        app = initApp;
        coursedata = new CourseData(initApp);
        projdata = new ProjData(initApp);
        recdata = new RecData(initApp);
        schdata = new SchData(initApp);
        tadata = new TAData(initApp);
    }
    @Override
    public void resetData() {
        coursedata.resetData();
        projdata.resetData();
        recdata.resetData();
        schdata.resetData();
        tadata.resetData();
    }
    public CourseData getCourseData(){
        return coursedata;
    }
    public ProjData getProjData(){
        return projdata;
    }
    public RecData getRecData(){
        return recdata;
    }
    public SchData getSchData(){
        return schdata;
    }
    public TAData getTAData(){
        return tadata;
    }
}
