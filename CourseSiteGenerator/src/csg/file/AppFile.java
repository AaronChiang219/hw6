/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.file;

import cm.data.CourseData;
import cm.data.CourseTemp;
import cm.file.CourseFile;
import csg.data.AppData;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import static djf.settings.AppPropertyType.EXPORT_COMPLETED_MESSAGE;
import static djf.settings.AppPropertyType.EXPORT_COMPLETED_TITLE;
import static djf.settings.AppPropertyType.EXPORT_ERROR_MESSAGE;
import static djf.settings.AppPropertyType.EXPORT_ERROR_TITLE;
import djf.ui.AppMessageDialogSingleton;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javax.imageio.ImageIO;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import org.apache.commons.io.FileUtils;
import pm.file.ProjFile;
import properties_manager.PropertiesManager;
import rm.file.RecFile;
import sm.file.SchFile;
import tam.CourseSiteGeneratorApp;
import tam.file.TAFiles;

/**
 *
 * @author Aaron
 */
public class AppFile implements AppFileComponent {
    CourseSiteGeneratorApp app;
    CourseFile coursefile;
    ProjFile projfile;
    RecFile recfile;
    SchFile schfile;
    TAFiles tafile;
    
    public AppFile(CourseSiteGeneratorApp initApp){
        app = initApp;
        coursefile = new CourseFile(initApp);
        projfile = new ProjFile(initApp);
        recfile = new RecFile(initApp);
        schfile = new SchFile(initApp);
        tafile = new TAFiles(initApp);
    }

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
       AppData dataManager = (AppData)data;
       coursefile.saveData(dataManager.getCourseData(), filePath);
       projfile.saveData(dataManager.getProjData(), filePath);
       recfile.saveData(dataManager.getRecData(), filePath);
       schfile.saveData(dataManager.getSchData(), filePath);
       tafile.saveData(dataManager.getTAData(), filePath);
       
       JsonObject json = Json.createObjectBuilder()
               .add("Course", coursefile.getFileData())
               .add("Proj", projfile.getFileData())
               .add("Rec",recfile.getFileData())
               .add("Sch", schfile.getFileData())
               .add("Ta", tafile.getFileData())
               .build();

        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(json);
        jsonWriter.close();

        // INIT THE WRITER
        OutputStream os = new FileOutputStream(filePath, true);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(json);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();
        os.close();
       
    }

    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        AppData dataManager = (AppData)data;
        coursefile.loadData(dataManager.getCourseData(), filePath);
        projfile.loadData(dataManager.getProjData(), filePath);
        tafile.loadData(dataManager.getTAData(), filePath);
        recfile.loadData(dataManager.getRecData(),filePath);
        schfile.loadData(dataManager.getSchData(), filePath);
        
    }

    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        AppData dataManager = (AppData)data;
        //Also since we only get the folder, we need to set up the subfolders ourselves
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        CourseData cdata = dataManager.getCourseData();
        if (cdata.getExport()==null || cdata.getSourceFolder()==null || cdata.getBanner() == null || cdata.getLFooter() == null || cdata.getRFooter() == null){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(EXPORT_ERROR_TITLE), props.getProperty(EXPORT_ERROR_MESSAGE));
        } else {
            filePath = cdata.getExport().getPath();
            File sourcefolder = cdata.getSourceFolder();
            File cssfolder = new File(filePath+"\\css");
            File exportimage = new File(filePath+"\\images");
            FileUtils.copyDirectory(sourcefolder, new File(filePath));
            if (cdata.getBanner()!=null){
                BufferedImage bi = SwingFXUtils.fromFXImage(cdata.getBanner(), null);
                ImageIO.write(bi, "png", new File(filePath+"\\images\\Banner.png"));
            }
            if (cdata.getLFooter()!=null){
                BufferedImage bi = SwingFXUtils.fromFXImage(cdata.getLFooter(), null);
                ImageIO.write(bi, "jpg", new File(filePath+"\\images\\LFooter.jpg"));
            }
            if (cdata.getRFooter()!=null){
               BufferedImage bi = SwingFXUtils.fromFXImage(cdata.getRFooter(), null);
               ImageIO.write(bi, "png", new File(filePath+"\\images\\RFooter.png"));
            }
            if (cdata.getmyCSS()!=null){
                FileUtils.copyFileToDirectory(cdata.getmyCSS(), cssfolder);
            }
            //ADD STUFF SO THAT IT TELLS WHICH ONES TO EXPORT, USE COURSEDATA
            ObservableList<CourseTemp> temp = cdata.getTemplate();
            if (temp.get(0).getUse()){
                coursefile.exportData(dataManager.getCourseData(), filePath + "\\js\\CourseData.json");
            } else {
                FileUtils.deleteQuietly(new File(filePath+"\\index.html"));
            }
            if (temp.get(1).getUse()){
                recfile.exportData(dataManager.getRecData(), filePath + "\\js\\RecitationsData.json");
                tafile.exportData(dataManager.getTAData(), filePath + "\\js\\OfficeHoursGridData.json");
            } else {
                FileUtils.deleteQuietly(new File(filePath+"\\syllabus.html"));
            }
            if (temp.get(2).getUse()){
                schfile.exportData(dataManager.getSchData(), filePath + "\\js\\ScheduleData.json");
            } else {
                FileUtils.deleteQuietly(new File(filePath+"\\schedule.html"));
            }
            if (temp.get(3).getUse()){
                schfile.exportData(dataManager.getSchData(), filePath + "\\js\\ScheduleData.json");
            } else {
                FileUtils.deleteQuietly(new File(filePath+"\\hws.html"));
            }
            if (temp.get(4).getUse()){
                projfile.exportData(dataManager.getProjData(), filePath + "\\js\\TeamsAndStudents.json");
            } else {
                FileUtils.deleteQuietly(new File(filePath+"\\projects.html"));
            }
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(EXPORT_COMPLETED_TITLE), props.getProperty(EXPORT_COMPLETED_MESSAGE));
        }
    }

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
