/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sm.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Aaron
 */
public class Item <E extends Comparable<E>> implements Comparable<E>{
    public final StringProperty type; 
    public final StringProperty date; 
    public final StringProperty title; 
    public final StringProperty topic; 
    public final StringProperty link;
    public final StringProperty criteria;
    public final StringProperty time;
    
    
    public Item(String initType, String initDate, String initTitle, String initTopic, String initLink, String initCriteria, String initTime){
        type = new SimpleStringProperty(initType);
        date = new SimpleStringProperty(initDate);
        title = new SimpleStringProperty(initTitle);
        topic = new SimpleStringProperty(initTopic);
        link = new SimpleStringProperty(initLink);
        criteria = new SimpleStringProperty(initCriteria);
        time = new SimpleStringProperty(initTime);
    }
    
    public String getTime(){
        return time.get();
    }
    public void setTime(String s){
        time.set(s);
    }
    public String getType(){
        return type.get();
    }
    public void setType(String s){
        type.set(s);
    }
    public String getDate(){
        return date.get();
    }
    public void setDate(String s){
        date.set(s);
    }
    public String getTitle(){
        return title.get();
    }
    public void setTitle(String s){
        title.set(s);
    }
    public String getTopic(){
        return topic.get();
    }
    public void setTopic(String s){
        topic.set(s);
    }
    public String getLink(){
        return link.get();
    }
    public void setLink(String s){
        link.set(s);
    }
    public String getCriteria(){
        return criteria.get();
    }
    public void setCriteria(String s){
        criteria.set(s);
    }

    @Override
    public int compareTo(E o) {
        return getTitle().compareTo(((Item)o).getTitle());
    }
}
