/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sm.data;

import djf.components.AppDataComponent;
import java.util.Collections;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import tam.CourseSiteGeneratorApp;

/**
 *
 * @author Aaron
 */
public class SchData implements AppDataComponent {
    CourseSiteGeneratorApp app;
    ObservableList<Item> schedule;
    String startDay;
    String startMonth;
    String endDay;
    String endMonth;
    
    public SchData(CourseSiteGeneratorApp initApp) {
        app = initApp;
        schedule = FXCollections.observableArrayList();
    }

    @Override
    public void resetData() {
        schedule.clear();
        startDay = "1";
        startMonth = "1";
        endDay = "2";
        endMonth = "2";
    }
    
    public void  setDates(String sd, String sm, String ed, String em){
        startDay = sd;
        startMonth = sm;
        endDay = ed;
        endMonth = em;
    }
    public String getStartDay(){
        return startDay;
    }
    public String getStartMonth(){
        return startMonth;
    }
    public String getEndDay(){
        return endDay;
    }
    public String getEndMonth(){
        return endMonth;
    }
    public ObservableList getSchedule(){
        return schedule;
    }
    
    public Item getItem(String title) {
        for (Item i : schedule) {
            if (i.getTitle().equals(title)) {
                return i;
            }
        }
        return null;
    }
    public boolean containsItem(String title) {
        for (Item i : schedule) {
            if (i.getTitle().equals(title)) {
                return true;
            }
        }
        return false;
    }
    
    public void addItem(String type, String date, String title, String topic, String link, String criteria, String time) {
        Item item = new Item(type, date, title, topic, link, criteria, time);
        if (!containsItem(title)) {
            schedule.add(item);
        }

        Collections.sort(schedule);
    }
    
    public void removeItem(String type, String date, String title, String topic, String link, String criteria, String time){
        for (Item i: schedule){
            if (i.getDate().equals(date) && i.getTitle().equals(title) && i.getTopic().equals(topic) && i.getTime().equals(time)){
                schedule.remove(i);
            }
        }
    }
}
