/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sm.file;

import csg.workspace.AppWorkspace;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import javafx.collections.ObservableList;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import sm.data.Item;
import sm.data.SchData;
import tam.CourseSiteGeneratorApp;

/**
 *
 * @author Aaron
 */
public class SchFile implements AppFileComponent {

    CourseSiteGeneratorApp app;
    
    JsonObject filedata;
    static final String JSON_STARTDAY = "startingMondayDay";
    static final String JSON_STARTMONTH = "startingMondayMonth";
    static final String JSON_ENDDAY = "endingFridayDay";
    static final String JSON_ENDMONTH = "endingFridayMonth";
    static final String JSON_SCHEDULE = "schedule";
    static final String JSON_TYPE = "type";
    static final String JSON_DATE = "date";
    static final String JSON_TITLE = "title";
    static final String JSON_TOPIC = "topic";
    static final String JSON_LINK = "link";
    static final String JSON_CRITERIA = "criteria";
    static final String JSON_TIME = "time";
    
    public SchFile(CourseSiteGeneratorApp initApp) {
        app = initApp;
    }

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        SchData dataManager = (SchData)data;
        
        JsonArrayBuilder scheduleArrayBuilder = Json.createArrayBuilder();
        ObservableList<Item> schedule = dataManager.getSchedule();
        for(Item i: schedule){
            JsonObject itemJson = Json.createObjectBuilder()
                    .add(JSON_TYPE, i.getType())
                    .add(JSON_DATE, i.getDate())
                    .add(JSON_TITLE, i.getTitle())
                    .add(JSON_TOPIC, i.getTopic())
                    .add(JSON_LINK, i.getLink())
                    .add(JSON_CRITERIA, i.getCriteria())
                    .add(JSON_TIME, i.getTime())
                    .build();
            scheduleArrayBuilder.add(itemJson);
        }
        JsonArray scheduleArray = scheduleArrayBuilder.build();
        
        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_STARTDAY, dataManager.getStartDay())
                .add(JSON_STARTMONTH, dataManager.getStartMonth())
                .add(JSON_ENDDAY, dataManager.getEndDay())
                .add(JSON_ENDMONTH, dataManager.getEndMonth())
                .add(JSON_SCHEDULE, scheduleArray)
                .build();
        filedata = dataManagerJSO;
    }

    public JsonObject getFileData(){
        return filedata;
    }
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        SchData dataManager = (SchData)data;
        
        JsonObject json = loadJSONFile(filePath);
        
        String startDay = json.getString(JSON_STARTDAY);
        String startMonth = json.getString(JSON_STARTMONTH);
        String endDay = json.getString(JSON_ENDDAY);
        String endMonth = json.getString(JSON_ENDMONTH);
        dataManager.setDates(startDay, startMonth, endDay, endMonth);
        
        ((AppWorkspace)app.getWorkspaceComponent()).getSchWorkspace().reloadWorkspace(dataManager);
        JsonArray jsonScheduleArray = json.getJsonArray(JSON_SCHEDULE);
        for(int i = 0; i<jsonScheduleArray.size(); i++){
            JsonObject jsonItem = jsonScheduleArray.getJsonObject(i);
            String type = jsonItem.getString(JSON_TYPE);
            String date = jsonItem.getString(JSON_DATE);
            String title = jsonItem.getString(JSON_TITLE);
            String topic = jsonItem.getString(JSON_TOPIC);
            String link = jsonItem.getString(JSON_LINK);
            String criteria = jsonItem.getString(JSON_CRITERIA);
            String time = jsonItem.getString(JSON_TIME);
            dataManager.addItem(type, date, title, topic, link, criteria, time);
        }
        ((AppWorkspace)app.getWorkspaceComponent()).getSchWorkspace().reloadWorkspace(dataManager);
    }

    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject().getJsonObject("Sch");
        jsonReader.close();
        is.close();
        return json;
    }
    
    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        SchData dataManager = (SchData) data;

        JsonArrayBuilder scheduleArrayBuilder = Json.createArrayBuilder();
        ObservableList<Item> schedule = dataManager.getSchedule();
        for (Item i : schedule) {
            JsonObject itemJson = Json.createObjectBuilder()
                    .add(JSON_TYPE, i.getType())
                    .add(JSON_DATE, i.getDate())
                    .add(JSON_TITLE, i.getTitle())
                    .add(JSON_TOPIC, i.getTopic())
                    .add(JSON_LINK, i.getLink())
                    .add(JSON_CRITERIA, i.getCriteria())
                    .add(JSON_TIME, i.getTime())
                    .build();
            scheduleArrayBuilder.add(itemJson);
        }
        JsonArray scheduleArray = scheduleArrayBuilder.build();

        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_STARTDAY, dataManager.getStartDay())
                .add(JSON_STARTMONTH, dataManager.getStartMonth())
                .add(JSON_ENDDAY, dataManager.getEndDay())
                .add(JSON_ENDMONTH, dataManager.getEndMonth())
                .add(JSON_SCHEDULE, scheduleArray)
                .build();
        
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataManagerJSO);
        jsonWriter.close();

        // INIT THE WRITER
        OutputStream os = new FileOutputStream(filePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();
    }

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
