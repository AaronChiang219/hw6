/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sm.workspace;

import csg.data.AppData;
import jtps.jTPS_Transaction;
import sm.data.SchData;
import tam.CourseSiteGeneratorApp;

/**
 *
 * @author caaro
 */
public class Date_Change_Transaction implements jTPS_Transaction{
    CourseSiteGeneratorApp app;
    String sd;
    String sm;
    String ed;
    String em;
    String osd;
    String osm;
    String oed;
    String oem;
    public Date_Change_Transaction(CourseSiteGeneratorApp a, String d, String m, String d2, String m2){
        app = a;
        SchData data = ((AppData)app.getDataComponent()).getSchData();
        sd = d;
        sm = m;
        ed = d2;
        em = m2;
        osd = data.getStartDay();
        osm = data.getStartMonth();
        oed = data.getEndDay();
        oem = data.getEndMonth();
    }
    @Override
    public void doTransaction() {
        SchData data = ((AppData)app.getDataComponent()).getSchData();
        data.setDates(sd, sm, ed, em);
    }

    @Override
    public void undoTransaction() {
        SchData data = ((AppData)app.getDataComponent()).getSchData();
        data.setDates(osd, osm, oed, oem);
    }
    
}
