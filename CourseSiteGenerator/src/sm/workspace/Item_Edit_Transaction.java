/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sm.workspace;
import csg.data.AppData;
import jtps.jTPS_Transaction;
import sm.data.Item;
import tam.CourseSiteGeneratorApp;
/**
 *
 * @author caaro
 */
public class Item_Edit_Transaction implements jTPS_Transaction{
    CourseSiteGeneratorApp app;
    private String type;
    private String date;
    private String title;
    private String topic;
    private String link;
    private String criteria;
    private String time;
    private Item item;
    private String otype;
    private String odate;
    private String otitle;
    private String otopic;
    private String olink;
    private String ocriteria;
    private String otime;
    public Item_Edit_Transaction(CourseSiteGeneratorApp a, Item itm, String ty, String da, String tit, String to, String li, String cr, String ti) {
        app = a;
        type = ty;
        date = da;
        title = tit;
        topic = to;
        link = li;
        criteria = cr;
        time = ti;
        item = itm;
        otype = item.getType();
        odate = item.getDate();
        otitle = item.getTitle();
        otopic = item.getTopic();
        olink = item.getLink();
        ocriteria = item.getCriteria();
        otime = item.getTime();
    }
    @Override
    public void doTransaction() {
       item.setCriteria(criteria);
       item.setDate(date);
       item.setLink(link);
       item.setTime(time);
       item.setTitle(title);
       item.setTopic(topic);
       item.setType(type);
       
    }

    @Override
    public void undoTransaction() {
        item.setCriteria(ocriteria);
        item.setDate(odate);
        item.setLink(olink);
        item.setTime(otime);
        item.setTitle(otitle);
        item.setTopic(otopic);
        item.setType(otype);

    }
    
}
