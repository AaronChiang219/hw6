/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sm.workspace;

import csg.data.AppData;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import java.time.LocalDate;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import jtps.jTPS;
import properties_manager.PropertiesManager;
import sm.data.Item;
import sm.data.SchData;
import tam.CourseSiteGeneratorApp;
import tam.CourseSiteGeneratorProp;

/**
 *
 * @author Aaron
 */
public class SchWorkspace extends AppWorkspaceComponent{
    CourseSiteGeneratorApp app;
    jTPS jTPS;
    SchController controller;
    VBox CalendarBox;
    VBox ScheduleBox;
    TilePane EditBox;
    
    Label StartDateLabel;
    Label EndDateLabel;
    
    DatePicker StartDatePicker;
    DatePicker EndDatePicker;
    
    TableView ScheduleGrid;
    
    Label CalLabel;
    Label SchLabel;
    Label EditLabel;
    Label TypeLabel;
    Label DateLabel;
    Label TimeLabel;
    Label TitleLabel;
    Label TopicLabel;
    Label LinkLabel;
    Label CriteriaLabel;
    
    ComboBox TypeCombo;
    DatePicker DatePicker;
    
    TextField TimeTextField;
    TextField TitleTextField;
    TextField TopicTextField;
    TextField LinkTextField;
    TextField CriteriaTextField;
    
    Button addButton;
    Button clearButton;
    
    BorderPane Schedule;
    public SchWorkspace(CourseSiteGeneratorApp initApp){
        app = initApp;
        jTPS = new jTPS();
        controller = new SchController(app);
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        SchData data = ((AppData)app.getDataComponent()).getSchData();
        CalendarBox = new VBox();
        CalendarBox.setMaxWidth(1000);
        CalendarBox.setStyle("-fx-border-color: black");
        CalendarBox.setAlignment(Pos.CENTER);
        
        CalLabel = new Label(props.getProperty(CourseSiteGeneratorProp.CALENDAR_TEXT.toString()));
        StartDateLabel = new Label(props.getProperty(CourseSiteGeneratorProp.START_DATE_TEXT.toString()));
        EndDateLabel = new Label(props.getProperty(CourseSiteGeneratorProp.END_DATE_TEXT.toString()));
        
        StartDatePicker = new DatePicker();
        StartDatePicker.setValue(LocalDate.now());
        EndDatePicker = new DatePicker();
        EndDatePicker.setValue(LocalDate.now().plusDays(4));
        data.setDates("2", "1", "3", "2");
        
        VBox headbox = new VBox();
        headbox.setStyle("-fx-background-color: #ccccff; -fx-border-color:black");
        headbox.setAlignment(Pos.CENTER);
        headbox.getChildren().add(CalLabel);
        CalendarBox.getChildren().add(headbox);
        
        HBox somebox = new HBox();
        somebox.setAlignment(Pos.CENTER);
        somebox.getChildren().add(StartDateLabel);
        somebox.getChildren().add(StartDatePicker);
        somebox.getChildren().add(new Label("                                 "));
        somebox.getChildren().add(EndDateLabel);
        somebox.getChildren().add(EndDatePicker);
        
        CalendarBox.getChildren().add(somebox);
        CalendarBox.getChildren().add(new Label("    "));
        ScheduleBox = new VBox();
        ScheduleBox.setMaxWidth(1000);
        //ScheduleBox.setStyle("-fx-border-color: black");
        ScheduleBox.setAlignment(Pos.CENTER);
        SchLabel = new Label(props.getProperty(CourseSiteGeneratorProp.SCHEDULE_TEXT.toString()));
        ScheduleGrid = new TableView();
        ObservableList<Item> scheduleData = data.getSchedule();
        ScheduleGrid.setItems(scheduleData);
        
        TableColumn TypeCol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.SCH_TYPE_COL.toString()));
        TypeCol.setCellValueFactory(new PropertyValueFactory<Item,String>("Type"));
        
        TableColumn DateCol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.SCH_DATE_COL.toString()));
        DateCol.setCellValueFactory(new PropertyValueFactory<Item, String>("Date"));
        
        TableColumn TitleCol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.SCH_TITLE_COL.toString()));
        TitleCol.setCellValueFactory(new PropertyValueFactory<Item,String>("Title"));
        
        TableColumn TopicCol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.SCH_TOPIC_COL.toString()));
        TopicCol.setCellValueFactory(new PropertyValueFactory<Item,String>("Topic"));
        
        ScheduleGrid.getColumns().addAll(TypeCol, DateCol, TitleCol, TopicCol);
        
        VBox anotherbox = new VBox();
        anotherbox.setStyle("-fx-background-color: #ccccff; -fx-border-color:black");
        anotherbox.setAlignment(Pos.CENTER);
        anotherbox.getChildren().add(SchLabel);
        ScheduleBox.getChildren().add(anotherbox);
        ScheduleBox.getChildren().add(ScheduleGrid);
        
        VBox SomeBox = new VBox();
        SomeBox.setMaxWidth(1000);
        SomeBox.setAlignment(Pos.CENTER);
        SomeBox.setStyle("-fx-border-color: black");
        //SomeBox.setMaxHeight(1000);
        
        EditBox = new TilePane();
        EditBox.setPrefColumns(2);
        EditBox.setMaxWidth(700);
        EditBox.setVgap(10);
        
        EditLabel = new Label(props.getProperty(CourseSiteGeneratorProp.ADD_UPDATE_BUTTON_TEXT.toString()));
        TypeLabel = new Label(props.getProperty(CourseSiteGeneratorProp.TYPE_TEXT.toString()));
        DateLabel = new Label(props.getProperty(CourseSiteGeneratorProp.DATE_TEXT.toString()));
        TimeLabel = new Label(props.getProperty(CourseSiteGeneratorProp.TIME_TEXT.toString()));
        TitleLabel = new Label(props.getProperty(CourseSiteGeneratorProp.TITLE_TEXT.toString()));
        TopicLabel = new Label(props.getProperty(CourseSiteGeneratorProp.TOPIC_TEXT.toString()));
        LinkLabel = new Label(props.getProperty(CourseSiteGeneratorProp.LINK_TEXT.toString()));
        CriteriaLabel = new Label(props.getProperty(CourseSiteGeneratorProp.CRITERIA_TEXT.toString()));
        
        TypeCombo = new ComboBox();
        TypeCombo.getItems().addAll("Holiday", "Lecture", "Recitation", "References", "HWs");
        DatePicker = new DatePicker();
        
        TimeTextField = new TextField();
        TitleTextField = new TextField();
        TopicTextField = new TextField();
        LinkTextField = new TextField();
        CriteriaTextField = new TextField();
        
        addButton = new Button(props.getProperty(CourseSiteGeneratorProp.ADD_UPDATE_BUTTON_TEXT.toString()));
        clearButton = new Button(props.getProperty(CourseSiteGeneratorProp.CLEAR_BUTTON_TEXT.toString()));
        
        VBox blah = new VBox();
        blah.setStyle("-fx-background-color: #ccccff; -fx-border-color:black");
        blah.setAlignment(Pos.CENTER);
        blah.getChildren().add(EditLabel);
        
        EditBox.getChildren().add(new Label("   "));
        EditBox.getChildren().add(new Label("    "));
        EditBox.getChildren().add(TypeLabel);
        EditBox.getChildren().add(TypeCombo);
        EditBox.getChildren().add(DateLabel);
        EditBox.getChildren().add(DatePicker);
        EditBox.getChildren().add(TimeLabel);
        EditBox.getChildren().add(TimeTextField);
        EditBox.getChildren().add(TitleLabel);
        EditBox.getChildren().add(TitleTextField);
        EditBox.getChildren().add(TopicLabel);
        EditBox.getChildren().add(TopicTextField);
        EditBox.getChildren().add(LinkLabel);
        EditBox.getChildren().add(LinkTextField);
        EditBox.getChildren().add(CriteriaLabel);
        EditBox.getChildren().add(CriteriaTextField);
        EditBox.getChildren().add(addButton);
        EditBox.getChildren().add(clearButton);
        EditBox.getChildren().add(new Label("   "));
        EditBox.getChildren().add(new Label("    "));
        
        VBox temp = new VBox();
        SomeBox.getChildren().addAll(blah, EditBox);
        temp.setAlignment(Pos.CENTER);
        temp.getChildren().addAll(CalendarBox, ScheduleBox, SomeBox);
        Schedule = new BorderPane();
        Schedule.setCenter(temp);
        addButton.setOnAction(e ->{
            controller.handleAddItem();
        });
        StartDatePicker.setOnAction(e -> {
            controller.handleChangeDates();
        });
        EndDatePicker.setOnAction(e -> {
            controller.handleChangeDates();
        });
        ScheduleGrid.setOnMouseClicked(e -> {
            controller.handleEditItem();
        });
        clearButton.setOnAction(e -> {
            controller.handleClearItem();
        });
        ScheduleGrid.setOnKeyPressed(e ->{
            controller.handleKeyPress(e.getCode());
        });
    }
    public BorderPane getSchedule(){
        return Schedule;
    }
    public Label getStartDateLabel(){
        return StartDateLabel;
    }
    public Label getEndDateLabel(){
        return EndDateLabel;
    }
    public DatePicker getStartDatePicker(){
        return StartDatePicker;
    }
    public DatePicker getEndDatePicker(){
        return EndDatePicker;
    }
    public TableView getScheduleGrid(){
        return ScheduleGrid;
    }
    public Label getCalLabel(){
        return CalLabel;
    }
    public Label getSchLabel(){
        return SchLabel;
    }
    public Label getEditLabel(){
        return EditLabel;
    }
    public Label getTypeLabel(){
        return TypeLabel;
    }
    public Label getDateLabel(){
        return DateLabel;
    }
    public Label getTimeLabel(){
        return TimeLabel;
    }
    public Label getTitleLabel(){
        return TitleLabel;
    }
    public Label getTopicLabel(){
        return TopicLabel;
    }
    public Label getLinkLabel(){
        return LinkLabel;
    }
    public Label getCriteriaLabel(){
        return CriteriaLabel;
    }
    
    public ComboBox getTypeCombo(){
        return TypeCombo;
    }
    public DatePicker getDatePicker(){
        return DatePicker;
    }
    public TextField getTimeTextField(){
        return TimeTextField;
    }
    public TextField getTitleTextField(){
        return TitleTextField;
    }
    public TextField getTopicTextField(){
        return TopicTextField;
    }
    public TextField getLinkTextField(){
        return LinkTextField;
    }
    public TextField getCriteriaTextField(){
        return CriteriaTextField;
    }
    public Button getaddButton(){
        return addButton;
    }
    public Button getclearButton(){
        return clearButton;
    }
    public jTPS getjTPS(){
        return jTPS;
    }
    public void setjTPS(jTPS j){
        jTPS = j;
    }
    @Override
    public void resetWorkspace() {
        setjTPS(new jTPS());
        getScheduleGrid().getColumns().clear();
    }

    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {
        SchData dataManager = (SchData)dataComponent;
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        getScheduleGrid().getColumns().clear();
        TableView scheduleGrid = getScheduleGrid();
        ObservableList<Item> scheduleData = dataManager.getSchedule();
        scheduleGrid.setItems(scheduleData);

        TableColumn TypeCol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.SCH_TYPE_COL.toString()));
        TypeCol.setCellValueFactory(new PropertyValueFactory<Item, String>("Type"));

        TableColumn DateCol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.SCH_DATE_COL.toString()));
        DateCol.setCellValueFactory(new PropertyValueFactory<Item, String>("Date"));

        TableColumn TitleCol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.SCH_TITLE_COL.toString()));
        TitleCol.setCellValueFactory(new PropertyValueFactory<Item, String>("Title"));

        TableColumn TopicCol = new TableColumn(props.getProperty(CourseSiteGeneratorProp.SCH_TOPIC_COL.toString()));
        TopicCol.setCellValueFactory(new PropertyValueFactory<Item, String>("Topic"));

        scheduleGrid.getColumns().addAll(TypeCol, DateCol, TitleCol, TopicCol);
    }
    
}
