/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sm.workspace;
import csg.data.AppData;
import jtps.jTPS_Transaction;
import sm.data.SchData;
import tam.CourseSiteGeneratorApp;
/**
 *
 * @author caaro
 */
public class Item_Delete_Transaction implements jTPS_Transaction{
    CourseSiteGeneratorApp app;
    private String type;
    private String date;
    private String title;
    private String topic;
    private String link;
    private String criteria;
    private String time;

    public Item_Delete_Transaction(CourseSiteGeneratorApp a, String ty, String da, String tit, String to, String li, String cr, String ti) {
        app = a;
        type = ty;
        date = da;
        title = tit;
        topic = to;
        link = li;
        criteria = cr;
        time = ti;
    }
    @Override
    public void doTransaction() {
        SchData data = ((AppData) app.getDataComponent()).getSchData();
        data.removeItem(type, date, title, topic, link, criteria, time);
    }

    @Override
    public void undoTransaction() {
        SchData data = ((AppData) app.getDataComponent()).getSchData();
        data.addItem(type, date, title, topic, link, criteria, time);
    }
    
}
