/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sm.workspace;
import csg.data.AppData;
import csg.workspace.AppWorkspace;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.scene.control.Button;
import djf.controller.AppFileController;
import djf.ui.AppGUI;
import static tam.CourseSiteGeneratorProp.*;
import djf.ui.AppMessageDialogSingleton;
import djf.ui.AppYesNoCancelDialogSingleton;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import jtps.jTPS;
import jtps.jTPS_Transaction;
import properties_manager.PropertiesManager;
import rm.data.RecData;
import rm.data.Recitation;
import sm.data.Item;
import sm.data.SchData;
import tam.CourseSiteGeneratorApp;
import tam.CourseSiteGeneratorProp;
/**
 *
 * @author caaro
 */
public class SchController {
    CourseSiteGeneratorApp app;
    
    public SchController(CourseSiteGeneratorApp initApp){
        app = initApp;
    }
    
    private void markWorkAsEdited(){
        AppGUI gui = app.getGUI();
        gui.getFileController().markAsEdited(gui);
    }
    
    public void handleChangeDates(){
        SchWorkspace workspace = ((AppWorkspace)app.getWorkspaceComponent()).getSchWorkspace();
        SchData data = ((AppData)app.getDataComponent()).getSchData();
        DatePicker start = workspace.getStartDatePicker();
        DatePicker end = workspace.getEndDatePicker();
        LocalDate startdate = start.getValue();
        LocalDate enddate = end.getValue();
        if (startdate.getDayOfWeek()!=DayOfWeek.MONDAY || enddate.getDayOfWeek()!=DayOfWeek.FRIDAY || startdate.isAfter(enddate)){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show("Error message to be filled in by xml later", "same");
        } else {
            String sd = startdate.toString();
            String ed = enddate.toString();
            jTPS_Transaction dates = new Date_Change_Transaction(app,sd.substring(8, 10),sd.substring(5, 7),ed.substring(8, 10),ed.substring(5, 7) );
            workspace.getjTPS().addTransaction(dates);
            //data.setDates(sd.substring(8,10), sd.substring(5,7), ed.substring(8,10), ed.substring(5,7));
        }
    }
    public void handleAddItem(){
        SchWorkspace workspace = ((AppWorkspace)app.getWorkspaceComponent()).getSchWorkspace();
        ComboBox typeBox = workspace.getTypeCombo();
        DatePicker datePick = workspace.getDatePicker();
        LocalDate datevalue = datePick.getValue();
        TextField titleTextField = workspace.getTitleTextField();
        TextField timeTextField = workspace.getTimeTextField();
        TextField topicTextField = workspace.getTopicTextField();
        TextField linkTextField = workspace.getLinkTextField();
        TextField criteriaTextField = workspace.getCriteriaTextField();
        String title = titleTextField.getText();
        String topic = topicTextField.getText();
        String type = typeBox.getValue().toString();
        String link = linkTextField.getText();
        String criteria = criteriaTextField.getText();
        String time = timeTextField.getText();
        String date = datevalue.toString();
        SchData data = ((AppData)app.getDataComponent()).getSchData();
        if (data.containsItem(titleTextField.getText())){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show("Error message to be filled in by xml later", "same");
        } else {
            jTPS_Transaction add = new Item_Add_Transaction(app, type, date, title, topic, link, criteria, time);
            workspace.getjTPS().addTransaction(add);
            typeBox.setValue("");
            titleTextField.setText("");
            timeTextField.setText("");
            topicTextField.setText("");
            linkTextField.setText("");
            criteriaTextField.setText("");
            markWorkAsEdited();
        }
    }
    public void handleEditItem(){
        SchWorkspace workspace = ((AppWorkspace)app.getWorkspaceComponent()).getSchWorkspace();
        TableView schTable = workspace.getScheduleGrid();
        Object selectedItem = schTable.getSelectionModel().getSelectedItem();
        if (selectedItem == null){
            return;
        }
        Item item = (Item)selectedItem;
        String time = item.getTime();
        String topic = item.getTopic();
        String title = item.getTitle();
        String link = item.getLink();
        String criteria = item.getCriteria();
        String date = item.getDate();
        String type = item.getType();
        TextField timeTextField = workspace.getTimeTextField();
        TextField topicTextField = workspace.getTopicTextField();
        TextField titleTextField = workspace.getTitleTextField();
        TextField linkTextField = workspace.getLinkTextField();
        TextField criteriaTextField = workspace.getCriteriaTextField();
        ComboBox typeBox = workspace.getTypeCombo();
        DatePicker datepick = workspace.getDatePicker();
        timeTextField.setText(time);
        topicTextField.setText(topic);
        titleTextField.setText(title);
        linkTextField.setText(link);
        criteriaTextField.setText(criteria);
        typeBox.setValue(type);
        datepick.setValue(LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        Button button = workspace.getaddButton();
        button.setOnAction(e ->{
            updateItem(item);
        });
        
    }
    public void updateItem(Item item){
        SchWorkspace workspace = ((AppWorkspace)app.getWorkspaceComponent()).getSchWorkspace();
        SchData data = ((AppData)app.getDataComponent()).getSchData();
        TextField timeTextField = workspace.getTimeTextField();
        TextField topicTextField = workspace.getTopicTextField();
        TextField titleTextField = workspace.getTitleTextField();
        TextField linkTextField = workspace.getLinkTextField();
        TextField criteriaTextField = workspace.getCriteriaTextField();
        ComboBox typeBox = workspace.getTypeCombo();
        DatePicker datepick = workspace.getDatePicker();
        String time = timeTextField.getText();
        String topic = topicTextField.getText();
        String title = titleTextField.getText();
        String link = linkTextField.getText();
        String criteria = criteriaTextField.getText();
        String type = (String) typeBox.getValue();
        String date = datepick.toString();
        if (data.containsItem(title) && !title.equals(item.getTitle())){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show("Error message to be filled in by xml later", "same");
        } else {
            jTPS_Transaction edit = new Item_Edit_Transaction(app, item, type, date, title, topic, link, criteria, time);
            workspace.getjTPS().addTransaction(edit);
            timeTextField.setText("");
            topicTextField.setText("");
            titleTextField.setText("");
            linkTextField.setText("");
            criteriaTextField.setText("");
            typeBox.setValue("");
            Button button = workspace.getaddButton();
            button.setOnAction(e -> {
                handleAddItem();
            });
            markWorkAsEdited();
        }
    }
    public void handleClearItem(){
        SchWorkspace workspace = ((AppWorkspace) app.getWorkspaceComponent()).getSchWorkspace();
        SchData data = ((AppData) app.getDataComponent()).getSchData();
        TextField timeTextField = workspace.getTimeTextField();
        TextField topicTextField = workspace.getTopicTextField();
        TextField titleTextField = workspace.getTitleTextField();
        TextField linkTextField = workspace.getLinkTextField();
        TextField criteriaTextField = workspace.getCriteriaTextField();
        ComboBox typeBox = workspace.getTypeCombo();
        DatePicker datepick = workspace.getDatePicker();
        timeTextField.setText("");
        topicTextField.setText("");
        titleTextField.setText("");
        linkTextField.setText("");
        criteriaTextField.setText("");
        typeBox.setValue("");
        Button button = workspace.getaddButton();
        button.setOnAction(e -> {
            handleAddItem();
        });
    }
    
    public void handleKeyPress(KeyCode key){
        SchWorkspace workspace = ((AppWorkspace) app.getWorkspaceComponent()).getSchWorkspace();
        TableView schTable = workspace.getScheduleGrid();
        Object selectedItem = schTable.getSelectionModel().getSelectedItem();
        if (selectedItem != null && key.equals(KeyCode.DELETE)) {
            Item item = (Item)selectedItem;
            String type = item.getType();
            String date = item.getDate();
            String title = item.getTitle();
            String topic = item.getTopic();
            String link = item.getLink();
            String criteria = item.getCriteria();
            String time = item.getTime();
            jTPS_Transaction del = new Item_Delete_Transaction(app, type, date, title, topic, link, criteria, time);
            workspace.getjTPS().addTransaction(del);
            markWorkAsEdited();
        }
    }
}
