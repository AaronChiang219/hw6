/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Aaron
 */
public class SaveTests {
    
    public SaveTests() {
        TestSave test = new TestSave();
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCourseData() throws IOException{
        String fileName = "C:\\Users\\Aaron\\Documents\\NetBeansProjects\\hw5\\CourseSiteGenerator\\SiteSaveTest.json";
        JsonObject json = loadJSONFile(fileName, "Course");
        System.out.println("Testing Course Data");
        assertEquals("CSE", json.getString("course"));
        assertEquals("219", json.getString("number"));
        assertEquals("Fall", json.getString("semester"));
        assertEquals("2017", json.getString("year"));
        assertEquals("Computer Science", json.getString("title"));
        
    }
    
    @Test
    public void testProjectData() throws IOException{
        String fileName = "C:\\Users\\Aaron\\Documents\\NetBeansProjects\\hw5\\CourseSiteGenerator\\SiteSaveTest.json";
        JsonObject json = loadJSONFile(fileName, "Proj");
        System.out.println("Testing Project Data");
        JsonObject team = json.getJsonArray("teams").getJsonObject(0);
        assertEquals("Tigers", team.getString("name"));
        assertEquals("#FFFFFF", team.getString("color"));
        assertEquals("#000000", team.getString("tcolor"));
        assertEquals("www.google.com", team.getString("link"));
        JsonObject person = json.getJsonArray("students").getJsonObject(0);
        assertEquals("Aaron", person.getString("fname"));
        assertEquals("Chiang", person.getString("lname"));
        assertEquals("Tigers", person.getString("team"));
        assertEquals("Lead Designer", person.getString("role"));
        
    }
    @Test
    public void testRecData() throws IOException{
        String fileName = "C:\\Users\\Aaron\\Documents\\NetBeansProjects\\hw5\\CourseSiteGenerator\\SiteSaveTest.json";
        JsonObject json = loadJSONFile(fileName, "Rec");
        System.out.println("Testing Recitation Data");
        JsonObject recitation = json.getJsonArray("recitations").getJsonObject(0);
        assertEquals("R05", recitation.getString("section"));
        assertEquals("Banerjee", recitation.getString("instructor"));
        assertEquals("locatin", recitation.getString("location"));
        assertEquals("time", recitation.getString("day_time"));
        assertEquals("tom", recitation.getString("ta_1"));
        assertEquals("tom", recitation.getString("ta_2"));
        
    }
    @Test
    public void testSchData() throws IOException{
        String fileName = "C:\\Users\\Aaron\\Documents\\NetBeansProjects\\hw5\\CourseSiteGenerator\\SiteSaveTest.json";
        JsonObject json = loadJSONFile(fileName, "Sch");
        System.out.println("Testing Schedule Data");
        
        assertEquals("1",json.getString("startingMondayDay"));
        assertEquals("2",json.getString("startingMondayMonth"));
        assertEquals("2",json.getString("endingFridayDay"));
        assertEquals("3",json.getString("endingFridayMonth"));
        JsonObject schedule = json.getJsonArray("schedule").getJsonObject(0);
        assertEquals("HW",schedule.getString("type"));
        assertEquals("2/3",schedule.getString("date"));
        assertEquals("HW 1",schedule.getString("title"));
        assertEquals("blah",schedule.getString("topic"));
    }
    @Test
    public void testTAData() throws IOException{
        String fileName = "C:\\Users\\Aaron\\Documents\\NetBeansProjects\\hw5\\CourseSiteGenerator\\SiteSaveTest.json";
        JsonObject json = loadJSONFile(fileName, "Ta");
        System.out.println("Testing TA Data");
        JsonObject TA = json.getJsonArray("undergrad_tas").getJsonObject(0);
        assertEquals("Aaron",TA.getString("name"));
        assertEquals("aaron@gmail.com", TA.getString("email"));
    }
    private JsonObject loadJSONFile(String jsonFilePath, String type) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject().getJsonObject(type);
        jsonReader.close();
        is.close();
        return json;
    }
}
