/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//Data to load
var course;
var number;
var year;
var semester;
var title;

function buildBanner() {
    var dataFile = "./js/CourseData.json";
    somethingData(dataFile, loadBanner);
}

function somethingData(jsonFile, callback) {
    $.getJSON(jsonFile, function (json) {
        callback(json);
    });
}

function loadBanner(data){
    var Banner = $("#banner");
    course = data.course;
    number = data.number;
    year = data.year;
    semester = data.semester;
    title = data.title;
    var text = course + " " + number + 
               " - " + semester + " " + year +
               "<br>" + title;
    Banner.prepend(text);
    
}


