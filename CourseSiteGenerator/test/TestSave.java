
import cm.data.CourseData;
import csg.data.AppData;
import csg.file.AppFile;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import pm.data.ProjData;
import rm.data.RecData;
import sm.data.SchData;
import tam.CourseSiteGeneratorApp;
import tam.data.TAData;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Aaron
 */
public class TestSave {
    AppData appdata;
    AppFile appfile;
    CourseData coursedata;
    ProjData projdata;
    RecData recdata;
    SchData schdata;
    TAData tadata;
    public TestSave() {
        CourseSiteGeneratorApp app = new CourseSiteGeneratorApp();
        appdata = new AppData(app);
        appfile = new AppFile(app);
        coursedata = appdata.getCourseData();
        projdata = appdata.getProjData();
        recdata = appdata.getRecData();
        schdata = appdata.getSchData();
        tadata = appdata.getTAData();
        coursedata.setCourseInfo("CSE","219","Fall", "Computer Science", "2017");
        projdata.addStudent("Aaron", "Chiang", "Tigers", "Lead Designer");
        projdata.addStudent("Baron", "Chiang", "Tigers", "Lead Programmer");
        projdata.addStudent("Caron", "Chiang", "Tigers", "Project Manager");
        projdata.addStudent("Daron", "Chiang", "Tigers", "Data Designer");
        projdata.addTeam("Tigers", "#FFFFFF", "#000000", "www.google.com");
        recdata.addRecitation("R05", "Banerjee", "time", "locatin", "tom", "tom");
        schdata.addItem("HW", "2/3", "HW 1", "blah");
        tadata.addTA("Aaron", "aaron@gmail.com");
        schdata.setDates("1", "2", "2", "3");
        try {
            appfile.saveData(appdata, "SiteSaveTest.json");
        } catch (IOException ex) {
            Logger.getLogger(TestSave.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static void main(String args[]){
        TestSave test = new TestSave();
    }
}
